# Copyright(c) 1998 Prosa Srl <prosa@prosa.it>
# Copyright(c) 1998,2001 Alessandro Rubini <rubini@linux.it>

# This file is LGPL (see COPYING.LIB)

# First of all, a simple "unknown" procedure
proc which cmd {
    foreach n "/bin /sbin /usr/bin /usr/sbin /usr/local/bin" {
        if [file executable $n/$cmd] {return $n/$cmd}
    }
    error "$cmd: no such command"
}

proc unknown args {
    set cmd [lindex $args 0]
    if ![catch {which $cmd} res] {
	return [eval exec $res [lrange $args 1 end]]
    }
    error $res
}

proc bgerror err {
    global errorInfo
    puts stderr "pid [pid]: Error \"$err\""
    puts stderr $errorInfo
}

# A useful tool for our daemons
proc default_opt {name value} {
    global options
    if ![info exists options($name)] {set options($name) $value}
}

# Replace the external command "cat"
if [catch {which cat}] {
    proc cat {args} {
	foreach n $args {
	    set F [open $n]
	    while {[gets $F string]>=0} {puts $string}
	    close $F
	}
    }
}

# Replace "free"
if [catch {which free}] {
    proc free {} {
	set F [open /proc/meminfo]
	gets $F string; gets $F string2
	close $F
	return "$string\n$string2"
    }
}

# Replace "grep"
if [catch {which grep}] {
    proc grep {expr args} {
	if [llength $args]<2 {set out {$string}} else {set out {$n:$string}}
	foreach n $args {
	    set F [open $n]
	    while {[gets $F string]>=0} {
		if [regexp $expr $string] {eval puts $out}
	    }
	    close $F
	}
    }
}


# Replace "ls"
if [catch {which ls}] {
    proc ls {args} {
	set ret ""
	foreach n $args {
	    foreach f [glob -nocomplain $n] {
		file lstat $f st
		append ret [format "%s %4o %2i  %5i %5i  %7i %s" \
			[string index $st(type) 0] [expr $st(mode)%010000] \
			$st(nlink) $st(uid) $st(gid) $st(size) $f]
		if ![catch "file readlink $f" link] {
		    append ret " -> $link"
		}
		append ret "\n"
	    }
	}
	return $ret
    }
}

# Replace the commands "cp", "rm", "mv"
if [catch {which cp}] {proc cp {args} {eval file copy $args}}
if [catch {which mv}] {proc mv {args} {eval file rename $args}}
if [catch {which rm}] {proc rm {args} {eval file delete $args}}

# Fake system call
proc sys_getpid {} {return [pid]}

# Simple ps (even if /bin/ps is there)
proc ps {} {
    foreach n [glob /proc/\[1-9\]*] {
	lappend list [file tail $n]
    }
    set list [lsort -int $list]
    foreach n $list {
	if ![catch {set F [open /var/run/$n]}] {
	    set name($n) [gets $F]
	    close $F
	}
    }
    puts "  User  PPid  Pid St Size   RSS  Name"
    puts " -------------------------------------"
    foreach n $list {
	if [catch {set F [open /proc/$n/status]}] continue; # process exited
	while {[gets $F string]>0} {
	    set v([lindex $string 0]) [lindex $string 1]
	}
	close $F
	if [info exists name($n)] {set v(Name:) $name($n)}
	puts -nonewline [format "%5i %5i %5i %s " \
		$v(Uid:) $v(PPid:) $v(Pid:) $v(State:)]
	if ![info exists v(VmSize:)] {
	    set v(VmSize:) [set v(VmRSS:) 0]
	}
	puts [format "%5i %5i %s" $v(VmSize:) $v(VmRSS:) $v(Name:)]
	unset v
    }
}

# Interact on a file
proc interact_oneline {ID prompt input output error exit} {
    global interact
    if [gets $input string]<0 {
	$exit
    }
    append interact($ID) "${string}\n"
    if [info complete  $interact($ID)] {
	set err [catch {uplevel #0 "eval \$interact($ID)"} result]
	set interact($ID) ""
	if [string length $result] {
	    puts [expr $err?"$output":"$error"] $result
	}
	catch {sys_wait nohang} res
	puts -nonewline $output $prompt
	flush $output
    }
}

proc interact {file prompt} {
    fconfigure $file -blocking 1
    fconfigure $file -buffering line

    #### recreate your std files
    sys_dup $file stdin
    sys_dup $file stdout
    sys_dup $file stderr
    close $file
   
    puts -nonewline $file $prompt; flush $file
    fileevent $file readable [list \
	    interact_oneline stdin $prompt stdin stdout stderr exit]
}

# MP (14-06-99)
# getAllowedHosts  - parse /etc/hosts.allow
proc getAllowedHosts {service {file "/etc/hosts.allow"}} {
	set allowed_ip {}

	set f [open $file]
	while {![eof $f]} {
		gets $f line
		if {[regexp $line {^ *\#}]} {
			continue
		}
		set exp "^${service} *: *(.+)"
		if {[regexp -nocase $exp $line dummy ip_list]} {
			set ip_list [string trim $ip_list]
			set ips [split $ip_list " "]
			foreach ip $ips {
				lappend allowed_ip $ip
			}

			unset dummy
			unset ip
		}
	}
	close $f
	return $allowed_ip
}

proc isAllowedHost {addr list} {
    if {![llength $list]} {return 1}
    foreach ip $list {
	if [string match $ip $addr] {
	    return 1
	}
    }
    return 0
}



# Halt and reboot
proc halt {} {
    sys_kill -USR1 1
}
proc reboot {} {
    sys_kill -USR2 1
}


# tail-f and cat-f spawn a process, they use two common procs
proc ettcl_handler F {
    if [gets $F string]<0 {close $F; exit}
    puts $string
}
proc ettcl_follow {file name flag} {
    if [sys_fork] return
    if [sys_fork] exit
    newcmdname "$name $file"
    set F [open $file] 
    if $flag {catch {seek $F -400 end}}
    fileevent $F readable "ettcl_handler $F"
}
proc tail-f {file} {
    ettcl_follow $file tail-f 1
}
proc cat-f {file} {
    ettcl_follow $file cat-f 0
}

# seteuid: set user id, execute command and then go back to old user id
proc seteuid {uid cmd} {
    if [catch {sys_seteuid $uid} old_uid] {error "$old_uid"}
    if [catch {$cmd} res] {error "$res"}
    if [catch {sys_seteuid $old_uid} tmp] {error "$tmp"}
    return "$res"
}

# seteugid: set user and group id, execute command and then go back to 
# old user and group id
proc seteugid {uid gid cmd} {
    if [catch {sys_setegid $gid} old_gid] {error "$old_gid"}
    if [catch {sys_seteuid $uid} old_uid] {error "$old_uid"}
    if [catch {$cmd} res] {error "$res"}
    if [catch {sys_seteuid $old_uid} tmp] {error "$tmp"}
    if [catch {sys_setegid $old_gid} tmp] {error "$tmp"}
    return "$res"
}


