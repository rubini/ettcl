#!/bin/ettclsh
# Commandd: a telnetd-like server that doesn't fork -*-tcl-*-
#   Copyright (c) 1998,2001   A. Rubini (rubini@linux.it)
# License: GNU GPL

if [file exists /etc/cmdd.cfg]       {source /etc/cmdd.cfg}
if [info exists options(cmdd:cfg)]   {source $options(cmdd:cfg)}
default_opt cmdd:name       "cmdd"
default_opt cmdd:port       2300
default_opt cmdd:tcp        1
default_opt cmdd:udp        1
default_opt cmdd:udptimeout 60
default_opt      logfile    "/var/log/ettcl.log"
default_opt cmdd:logfile    $options(logfile)
default_opt      logfmt     "%Y-%m-%d %H:%M:%S %Z"
default_opt cmdd:logfmt     $options(logfmt)
default_opt      hostsfile  "/etc/hosts.allow"
default_opt cmdd:hostsfile  $options(hostsfile)

proc bgerror args {global errorInfo; puts stderr "Error: $args\n$errorInfo\n"}

# can't use init.tcl::interact_oneline because stdout must be redirected
proc oneline sock {
    global cmdd
    if [gets $sock string]<0 {cmddclose $sock; return}
    append cmdd($sock) "${string}\n"
    if [info complete  $cmdd($sock)] {
	proc exit args "cmddclose $sock"
	sys_dup $sock stdout; # redirect
	set r [catch [list uplevel #0 $cmdd($sock)] result]
	if ![info exists cmdd($sock)] return
	set cmdd($sock) ""
	if [string length $result] {puts $sock $result}
	if $r {puts $sock "Error $r"}
	puts -nonewline $sock "% "
	flush $sock
    }
}

proc cmddclose sock {
    global cmdd options

    if ![catch {set F [open $options(cmdd:logfile) a]}] {
	puts $F "[xtime -format $options(cmdd:logfmt)] cmdd\
		TCP $cmdd($sock:who) logout [expr [xtime]-$cmdd($sock:time)]"
	close $F
    }
    unset cmdd($sock); unset cmdd($sock:who); unset cmdd($sock:time)
    sys_dup $cmdd(stdout) stdout; # the sock may be dup'd to stdout, close it
    catch {close $sock}
}

proc tcpaccept {sock host port} {
    global cmdd options

    set ok [isAllowedHost $host $cmdd(allowed_ip)]
    if ![catch {set F [open $options(cmdd:logfile) a]}] {
       if $ok {
           puts $F "[xtime -format $options(cmdd:logfmt)] cmdd\
                   TCP $host:$port login"
       } else {
           puts $F "[xtime -format $options(cmdd:logfmt)] cmdd\
                   TCP $host:$port refused"
       }
       close $F
    }
    if !$ok {
       close $sock
       return ""
    }
    if ![catch {set F [open $options(cmdd:logfile) a]}] {
	set cmdd($sock:who)  "$host:$port"
	set cmdd($sock:time) "[xtime]"
	puts $F "[xtime -format $options(cmdd:logfmt)] cmdd\
		TCP $host:$port login"
	close $F
    }
    fconfigure $sock -blocking 1
    fconfigure $sock -buffering line
  
    puts -nonewline $sock "% "; flush $sock
    fileevent $sock readable "oneline $sock"
}

#UDP
proc udpaccept {sock from} {
    global cmdd options
    scan [split $from :] "%s %s" host port
    set ok [isAllowedHost $host $cmdd(allowed_ip)]
    if ![catch {set F [open $options(cmdd:logfile) a]}] {
       if $ok {
           puts $F "[xtime -format $options(cmdd:logfmt)] cmdd\
                   UDP $host:$port login"
       } else {
           puts $F "[xtime -format $options(cmdd:logfmt)] cmdd\
                   UDP $host:$port refused"
       }
       close $F
    }
    if !$ok {
	udp sendto $sock $from "."
	return 0
    }

    set cmdd($sock:$from:time) "[xtime]"
    return 1
}

proc oneudpline sock {
    global cmdd errorInfo
    set string [string trimright [udp recvfrom $sock from] .]
    if ![info exists cmdd($sock:$from)] {
	if ![udpaccept $sock $from] return
    } else {
	after cancel $cmdd($sock:$from:after)
    }
    set cmdd($sock:$from:after) \
	    [after $cmdd(udptimeout) [list cmddudpclose $sock $from timeout]]
    append cmdd($sock:$from) "${string}"
    if [info complete  $cmdd($sock:$from)] {
	proc exit args [list cmddudpclose $sock $from logout]
	# redisrect stdout
	set T [open $cmdd(tmp):$from w]; sys_dup $T stdout
	set r [catch [list uplevel #0 $cmdd($sock:$from)] result]
	sys_dup $cmdd(stdout) stdout; close $T
	if ![info exists cmdd($sock:$from)] return
	set cmdd($sock:$from) ""
	set F [open $cmdd(tmp):$from r]
	while {[gets $F line]>=0} {
	    udp sendto $sock $from "${line}\n."
	}
	close $F; #file delete $cmdd(tmp):$from
	if [string length $result] {
	    udp sendto $sock $from "${result}\n."
	}
	if $r {udp sendto $sock $from  "$errorInfo\nError $r\n."}
	udp sendto $sock $from  "% ."
    }
}


proc cmddudpclose {sock from msg} {
    global cmdd options
    if ![catch {set F [open $options(cmdd:logfile) a]}] {
	puts $F "[xtime -format $options(cmdd:logfmt)] cmdd\
		UDP $from $msg [expr [xtime]-$cmdd($sock:$from:time)]"
	close $F
    }
    udp sendto $sock $from "."
    catch {after cancel $cmdd($sock:$from:after)}
    foreach n [array names cmdd $sock:$from*] {unset cmdd($n)}
}




# TCP
if $options(cmdd:tcp) {
    set cmdd(serversock) [socket -server tcpaccept $options(cmdd:port)]
}
# UDP
if $options(cmdd:udp) {
    set cmdd(udp) [udp open $options(cmdd:port)]
    fileevent $cmdd(udp) readable "oneudpline $cmdd(udp)"
    set cmdd(tmp) "/tmp/cmmd.[pid]"
    set cmdd(udptimeout) [expr 1000 * $options(cmdd:udptimeout)]
}
set cmdd(allowed_ip) \
	[getAllowedHosts $options(cmdd:name) $options(cmdd:hostsfile)]

# save a copy of stdout
set cmdd(stdout) [open /dev/null w]
sys_dup stdout $cmdd(stdout); 
# done
set forever ""; vwait forever
