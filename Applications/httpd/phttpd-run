#!/bin/sh
# -*-tcl-*- \
exec ettclsh $0 $*

# This is a wrapper to set some options before running phttpd,
# the parsing-httpd for etlinux.
# In a real etlinux system you'll have all such assignments in /etc/options,
# so they are set once at system boot and stay forever. To test
# in a real system, running this file is a good approach (it sources phttpd
# at the end, but looking for it in the current directory).

# Since phttpd is an alternative to httpd, all options are still
# called "httpd:*". If you want to run them both at once, you'll need
# to do some search-and-replace for your specfici system

# NOTE: most of this file is the same as httpd-run, with only the "parse" part
# added at the end

# use the curernt directory by default, to ease testing
default_opt httpd:docroot [pwd]
default_opt httpd:port 8080

# set a few types, to make most things run smoothly
set options(httpd:type:.gif) image/gif
set options(httpd:type:.png) image/png
set options(httpd:type:.jpeg) image/jpeg
set options(httpd:type:.shtml) text/html

# Also, you can set filenames for error messages
# set options(http:403) /var/lib/error403.html

# Otherwise, you can parse your mime.types:
if ![catch {set F [open /etc/mime.types]}] {
    while {[gets $F str]>=0} {
	if [regexp "^ *#" $str] continue
	if [catch {set type [lindex $str 0]}] continue; # not valid list
	for {set n 1} \$n<[llength $str] {incr n} {
	    set options(httpd:type:.[lindex $str $n]) $type
	}
    }
    close $F
}

### Parsing rules

# In addition to parsing, we must set the list of index.html files
set options(httpd:index) "index.html index.shtml"

# Similarly to what happens above for :type:, the :parse: array is applied
# to file names. However, for better flexibility (since you are assumed
# to have enough processing power, if you choose the parsing daemon),
# regexps are used instead. If a subexpression is parenthesized, it's
# used as query string. You'll most likely want to use "(\?.*)?" before
# the end-of-string marker. You'll most likely want to use braces
# around the variable name, to prevent extra backslashes

# Example: run all ".php" files through an external cgi, printing the "OK"
# use "test.php" in this directory to try it. Accept optional query string
set {options(httpd:parse:^(.*\.php)(\?.*)?$)}  {/usr/bin/php3 1}
set {options(httpd:parse:^(.*\.php4)(\?.*)?$)}  {/usr/bin/php4 1}

# Example: run all ".tcl" files through a cgi, without printing the "OK"
# use "test.tcl" in this directory to try it. Accept query string.
set {options(httpd:parse:^(.*\.tcl)(\?.*)?$)}  {/usr/bin/tclsh 0}

# Example: filter all ".shtml" files through a tcl procedure, defined later.
# use "test.shtml" in this directory to try it
set {options(httpd:parse:^(.*\.shtml)$)}  {shtml_parse 0}

# the procedure already runs in a child process, with stdout as a socket
proc shtml_parse {file} {
    set dir [file dirname $file]
    if [catch {set F [open $file r]}] {
	hterror stdout 404 "Not Found" "open $file"
	exit 1
    }
    puts "HTTP/1.0 200 OK\n"
    while {[gets $F s]>=0} {
	if ![regexp "<!--.*#include.*virtual.*=.*\"(\[^\"\]*)\"" $s foo inc] {
	    puts $s
	    continue
	}
	set inc "$dir/$inc"
	# process an include file, without recursive parsing
	
	if [catch {set G [open $inc r]} err] {
	    puts "<p><b>Can't open $inc: $err</b></p>"
	    continue
	}
	puts "<!-- begin included file: $inc -->"
	fcopy $G stdout
	puts "<!-- end included file: $inc -->"
	close $G
    }
    exit 0
}

source ./phttpd

