/* Prosa system-call addons to tclsh for ET-Linux
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini (rubini@linux.it)
 * Copyright (c) 1999              Marco Pantaleoni (panta@prosa.it)
 * Copyright (c) 1998,1999         Prosa Srl (prosa@prosa.it)
 * Copyright (c) 2001              Rodolfo Giometti (giometti@linux.it)
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/ioctl.h>

#ifdef __GLIBC__
#include <sys/reboot.h>
#endif

#include "ettclInt.h"

#ifdef TCL_USE_PROSA_EXT

/* This vector is used by sys_signal command */
static struct signal_data {
    int active;
    int pid;
    Tcl_AsyncHandler token;
    Tcl_DString command;
    Tcl_Interp *interp;
} signal_data[NSIG];

/* This vector is used to convert between signals names and values */
static struct {
    char *name;
    int val;
} signal_table[] = {
   {"HUP",   SIGHUP},
   {"INT",   SIGINT},
   {"ILL",   SIGILL},
   {"TERM",  SIGTERM},
   {"SEGV",  SIGSEGV},
   {"PIPE",  SIGSEGV},
   {"USR1",  SIGUSR1},
   {"USR2",  SIGUSR2},
   {"CHLD",  SIGCHLD},
   {"IO",    SIGIO},
   {"WINCH", SIGWINCH},
   {NULL, 0},
};

/* Convert from signal name (string) to the relative signal number */
static int signal_name2num(Tcl_Interp *interp, char *sig_name)
{
    int i;
 
    for (i = 0; signal_table[i].val; i++)
        if (strcmp(sig_name, signal_table[i].name) == 0)
            return signal_table[i].val;

    /* otherwise, try a number */
    if (Tcl_GetInt(interp, sig_name, &i) != TCL_OK) i=-1;
    if (i >= NSIG) i = -1;
    return i;
}

/* Convert from signal number to the relative signal name (string) */
static char *signal_num2name(const int val)
{
    int i;
    static char buf[4];

    if (val < 0 || val >= NSIG) return NULL;

    for (i = 0; signal_table[i].val; i++)
        if (signal_table[i].val == val)
            return signal_table[i].name;
    sprintf(buf, "%d", val);
    return buf;
}

/* This procedure is called for each registered signal by using `sys_signal' */
static void signal_handler(int signo)
{
    /* We simply mark the TCL signal handler, and remember the pid */
    signal_data[signo].pid = getpid();
    Tcl_AsyncMark(signal_data[signo].token);
}

/* This procedure is the TCL signal handler */
static int async_proc(ClientData clientData, Tcl_Interp *c_interp, int c_code)
{
    struct signal_data *sd = (struct signal_data *)clientData;
    Tcl_Interp *interp = sd->interp;
    char *cmd = Tcl_DStringValue(&sd->command);
    char *result;
    int code;

    if (sd->pid != getpid())
	return TCL_OK;

     /* Set the `tcl_signal' variable */
    Tcl_SetVar(interp, "tcl_signal",
	       signal_num2name(sd - signal_data), TCL_GLOBAL_ONLY);

    /* Save interp->result (which is a char pointer) */
    result = strdup(interp->result);
    Tcl_SetResult(interp, "", TCL_STATIC);
 
    /* Execute the command */
    code = Tcl_GlobalEval(interp, cmd);

    /* reset the previous result */
    Tcl_SetResult(interp, result, (Tcl_FreeProc *)free);
 
    /* Unset the `tcl_signal' variable */
    Tcl_UnsetVar(interp, "tcl_signal", TCL_GLOBAL_ONLY);
 
    /* If there are an already defined interpreter... */
    if (c_interp)
       return c_code;   /* ...return the signaled commad's exit code */
    else
       return code;    /* ...return the current exit code */
}

/* this procedure is used to open a tty master and a tty slave */
static int open_tty_pair(int fds[2])
{
    char devname[] = "/dev/pty--";
    int i,j;
    char s1[]="pqrst";
    char s2[]="0123456789abcdef";

    /* look for a free master tty */
    fds[0]=-1;
    for (i=0; fds[0]<0 && i<5; i++)
	for (j=0; fds[0]<0 && j<16; j++) {
	    devname[8] = s1[i];
	    devname[9] = s2[j];
	    if ((fds[0] = open(devname, O_RDWR)) < 0) {
		if (errno == EIO) continue;
		i=5; break; /* break twice */
	    }
	}
    if (fds[0] < 0) return -1;

    devname[5] = 't'; /* slave */
    if ((fds[1] = open(devname, O_RDWR)) < 0) {
	close(fds[0]);
	return -1;
    }
    return 0;
}

static int get_fd_from_channel(Tcl_Channel channel, int dir1, int dir2)
{
    Tcl_File f;

    f = Tcl_GetChannelFile(channel, dir1);
    if (!f) f = Tcl_GetChannelFile(channel, dir2);
    return (int)Tcl_GetFileInfo(f, NULL); /* segfault if no f is there */
}

/* Simple wrappers to system calls */

int Tcl_ProsaSysCmd(ClientData dummy, Tcl_Interp *interp, 
		    int argc, char **argv)
{
    int i, j, k, pid, localint = SIGTERM;
    static char string[16];
    int  pipes[2];
    Tcl_File files[2];
    Tcl_Channel channels[2];
    int modes[2];
    int fd0, fd1, fcopy_once, creating_pipes;
    static unsigned char buffer[1024];

    switch(argv[0][4]) {

      case 's':
      {
          switch(argv[0][5]) {
	    case 'e':
	    {
		/*
		 * sys_setuid(), sys_seteuid(), sys_setgid(), sys_setegid()
		 *
		 * Usage:
		 *  sys_set[e]uid <uid>
		 *  sys_set[e]gid <gid>
		 *  uid = new user id
		 *  gid = new user id
		 * Returns old user/group id
		 */
		typedef int (*setfptr)();
		setfptr setf;
		uid_t old_uid;
		gid_t old_gid;
		int effective=0, group=0, tmp=7, new_id;
		const char *what;
		char str[40];
		if(argv[0][7]=='e') {
		    effective=1;
		    tmp=8;
		}
		group=argv[0][tmp]=='g' ? 1 : 0;
		if(group) {
		    setf = effective ? setegid : setgid;
		    what = "group";
		}
		else {
		    setf = effective ? seteuid : setuid;
		    what = "user";
		}
		if(argc != 2)
		    goto wna;
		if (Tcl_GetInt(interp, argv[1], &new_id) != TCL_OK) {
		    sprintf(str, "\nwhile parsing %s id", what);
		    Tcl_AppendResult(interp, str,NULL);
		    return TCL_ERROR;
		}
		if(group)
		    old_gid = getgid();
		else
		    old_uid = getuid();
		if(setf(new_id) < 0) {
		    Tcl_SetResult(interp, strerror(errno), NULL);
		    return TCL_ERROR;
		}
		sprintf(string, "%d", group ? old_gid : old_uid);
		Tcl_SetResult(interp, string, TCL_VOLATILE);
		return TCL_OK;
	    }
	    case 'y':                                   /* sys_sync */
                if (argc>1) goto wna;
                sync();
                return TCL_OK;

            case 'i':                                   /* sys_signal */
            {
                /*
                 * sys_signal
                 *
                 * Usage:
                 *   sys_signal <sig_name> <handler>
                 *
                 *   sig_name      - signal name (ex. CHLD)
                 *   handler       - a TCL command or DEFAULT or IGNORE
                 */
      
                int signo;
                struct sigaction act;
      
                if (argc != 3)
		    goto wna;
      
                /* Ok, check the signal name */
                if ((signo = signal_name2num(interp, argv[1])) < 0) {
		    Tcl_ResetResult(interp);
                    Tcl_AppendResult(interp, "sys_signal: invalid signal \"",
                                  argv[1], "\"", NULL);
                    return TCL_ERROR;
                }

		if (signal_data[signo].active) {
		    Tcl_AsyncDelete(signal_data[signo].token);
		    Tcl_DStringFree(&signal_data[signo].command);
		    signal_data[signo].active = 0;
		}
                /* Some settings... */
                sigemptyset(&act.sa_mask);
                act.sa_flags = SA_RESTART;   /* needed by TCL main loop */

                if (!strcmp(argv[2], "DEFAULT")) {
		    act.sa_handler = SIG_DFL;
		} else if (!strcmp(argv[2], "IGNORE")) {
		    act.sa_handler = SIG_IGN;
		} else {
		    /* Register the TCL signal handler and the TCL command
		       to execute */
		    signal_data[signo].active = 1;
		    signal_data[signo].token =
			Tcl_AsyncCreate(async_proc,
					(ClientData)(signal_data+signo));
		    Tcl_DStringInit(&signal_data[signo].command);
		    Tcl_DStringAppend(&signal_data[signo].command, argv[2],-1);
		    signal_data[signo].interp = interp;
		    act.sa_handler = signal_handler;
		}
		/* Install the signal handler */
                if (sigaction(signo, &act, NULL) < 0) {
		    Tcl_ResetResult(interp);
                    Tcl_AppendResult(interp, "sys_signal: ", strerror(errno),
				     NULL);
                    return TCL_ERROR;
                }
                return TCL_OK;
             }
          }
      }


      case 'k':                                   /* sys_kill */
        /* Decode the signal name */
        if (argc>1 && argv[1][0]=='-') {
	    if ((localint = signal_name2num(interp, argv[1]+1)) < 0) {
                Tcl_AppendResult(interp, "sys_kill: invalid signal \"",
				 argv[1]+1,"\"", NULL);
                return TCL_ERROR;
            }
            argv++, argc--;
        }
        if (argc<2) goto wna;
        for (i=1; i<argc; i++) {
            if (Tcl_GetInt(interp, argv[i], &pid) != TCL_OK) {
                Tcl_AppendResult(interp, "\nwhile parsing a pid", NULL);
                return TCL_ERROR;
            }
            if (kill(pid, localint)) {
                Tcl_AppendResult(interp, "sys_kill: ", strerror(errno),
                                 NULL);
                return TCL_ERROR;
            }
        }
        return TCL_OK;

      case 'n':                                   /* sys_nice */
        if (argc!=2) goto wna;
        /* Decode the prio name */
        if (Tcl_GetInt(interp, argv[1], &localint) != TCL_OK) {
            Tcl_AppendResult(interp, "\nwhile parsing the nice level",
                             NULL);
            return TCL_ERROR;
            }
        if (nice(-localint)) {
            Tcl_AppendResult(interp, "sys_nice: ", strerror(errno),
                             NULL);
            return TCL_ERROR;
        }
        return TCL_OK;

      case 'f':                                   /* sys_fork */
        if (argc>1) goto wna;
        if((localint=fork())<0) {
            Tcl_AppendResult(interp, "sys_fork: ", strerror(errno),
                             NULL);
            return TCL_ERROR;
        }
        sprintf(string, "%i", localint);
        Tcl_SetResult(interp, string, TCL_VOLATILE);
        return TCL_OK;

      case 'e':                                   /* sys_exec */
        if (argc==1) goto wna;
        execvp(argv[1],argv+1); /* If it returns, we are on error */
        Tcl_AppendResult(interp, argv[1], ": ", strerror(errno),
                         NULL);
        return TCL_ERROR;
            
      case 'w':                                   /* sys_wait */
        if (argc>2) goto wna;
        if (argc==1) {
            localint = wait(&i);
        } else if (argc==2 && !strcmp(argv[1],"nohang")) {
	    localint = waitpid(-1, &i, WNOHANG);
	    if (localint == 0) {
		Tcl_AppendResult(interp, "sys_wait: ", strerror(EAGAIN),
				 NULL);
		return TCL_ERROR;
	    }		
	} else {
            int pid;

            if (Tcl_GetInt(interp, argv[1], &pid) != TCL_OK) {
                Tcl_AppendResult(interp, "\nwhile parsing a pid",
                                 NULL);
                return TCL_ERROR;
            }
            localint = waitpid(pid, &i, 0);
        }
        if (localint<0) {
            Tcl_AppendResult(interp, "sys_wait: ", strerror(errno),
                             NULL);
            return TCL_ERROR;
	}
        sprintf(string,"%i %i",localint,WEXITSTATUS(i));
        Tcl_SetResult(interp, string, TCL_VOLATILE);
        return TCL_OK;

      case 'p':                                   /* sys_pipe */
      case 't':                                   /* sys_ttypair */
        if (argc!=1 && argc!=3) goto wna;
	if (argv[0][4] == 'p')
	    creating_pipes = 1;
	else
	    creating_pipes = 0;

	if (creating_pipes)
	    i = pipe(pipes);
	else
	    i = open_tty_pair(pipes);
        if (i) {
            Tcl_AppendResult(interp, argv[0], ": ", strerror(errno),
                             NULL);
            return TCL_ERROR;
        }

        /* Create the extra stuff used internally by Tcl */
	modes[0] = TCL_READABLE | (creating_pipes ? 0 : TCL_WRITABLE);
	modes[1] = TCL_WRITABLE | (creating_pipes ? 0 : TCL_READABLE);
	channels[0] = Tcl_MakeFileChannel((ClientData)pipes[0],
					  (ClientData)pipes[0], modes[0]);
	channels[1] = Tcl_MakeFileChannel((ClientData)pipes[1],
					  (ClientData)pipes[1], modes[1]);
	Tcl_RegisterChannel(interp, channels[0]);
	Tcl_RegisterChannel(interp, channels[1]);
			    
	if (argc == 1) {
	    Tcl_AppendResult(interp, Tcl_GetChannelName(channels[0]), " ",
			     Tcl_GetChannelName(channels[1]), NULL);
	} else /* 3 */ {
	    Tcl_SetVar(interp, argv[1], Tcl_GetChannelName(channels[0]), 0);
	    Tcl_SetVar(interp, argv[2], Tcl_GetChannelName(channels[1]), 0);
	}
        return TCL_OK;

      case 'o':                                   /* sys_opentty */
	if (argc!=1) goto wna;
	setsid();
	i = ioctl(0, TIOCSCTTY, 0); /* use stdin */
	if (i<0) {
            Tcl_AppendResult(interp, "sys_opentty: ", strerror(errno),
                             NULL);
	    return TCL_ERROR;
	}
	return TCL_OK;

      case 'r':                                   /* sys_reboot */
        if (getpid()!=1) { /* Not init */
            Tcl_AppendResult(interp, "sys_reboot: only init can do that",
                             NULL);
            return TCL_ERROR;
        }
	if (argc!=2) goto wna;
	if (!strcmp(argv[1],"reboot")) {
	    sync(); sleep(1);
#ifdef __GLIBC__
	    reboot(RB_AUTOBOOT);
#else
	    reboot(0xfee1dead, 672274793, 0x1234567);
#endif
            Tcl_AppendResult(interp, "sys_reboot: ", strerror(errno),
                             NULL);
            return TCL_ERROR;
        }
	if (!strcmp(argv[1],"halt")) {
            kill(SIGTERM,-1); kill(SIGKILL,-1);
            sync(); sleep(1); exit(0);
        }
        Tcl_AppendResult(interp, "sys_reboot: wrong option ",argv[1],NULL);
        return TCL_ERROR;
	
      case 'd':                                   /* sys_dup */
	if (argc!=3) goto wna;

	/* Both files must exist (quite a misfeature) */
	channels[0] = Tcl_GetChannel(interp, argv[1], &modes[0]);
	channels[1] = Tcl_GetChannel(interp, argv[2], &modes[1]);
        if ( (channels[0] == (Tcl_Channel) NULL)
	    || (channels[1] == (Tcl_Channel) NULL) ) {
            return TCL_ERROR;
        }

	/*
	 * The new file must be at least as open as the old one
	 */
	if ( ((modes[1]&TCL_READABLE) && !(modes[0]&TCL_READABLE))
	    || ((modes[1]&TCL_WRITABLE) && !(modes[0]&TCL_WRITABLE)) ) {
	    Tcl_AppendResult(interp,"Stream \"",argv[1],
			     "\"is not compatible with stream \"",argv[2],
			     "\"",NULL);
	    return TCL_ERROR;
	}

	/* Retrieve old fd */
	fd1 = get_fd_from_channel(channels[1], TCL_READABLE, TCL_WRITABLE);
	/* Now duplicate the source fd (assume it is unique) */
	fd0 = get_fd_from_channel(channels[0], TCL_READABLE, TCL_WRITABLE);

	if (dup2(fd0,fd1)<0) {
	    Tcl_AppendResult(interp,argv[0],": dup2() failed", NULL);
	    return TCL_ERROR;
	}
	return TCL_OK;

      case 'y':                                   /* fcopy */
	if (argc!=3 && argc!=4) goto wna;
	if (argc==4) fcopy_once = 1; else fcopy_once = 0;
	/* Both files must exist */
	channels[0] = Tcl_GetChannel(interp, argv[1], &modes[0]);
	channels[1] = Tcl_GetChannel(interp, argv[2], &modes[1]);
        if ( (channels[0] == (Tcl_Channel) NULL)
	    || (channels[1] == (Tcl_Channel) NULL) ) {
            return TCL_ERROR;
        }
	/*
	 * The first must be readable and the second must be writable
	 */
	if ( !(modes[0]&TCL_READABLE)
	    || !(modes[1]&TCL_WRITABLE) ) {
	    Tcl_AppendResult(interp, argv[1]," must be readable and ",
			     argv[2], " must be writable", NULL);
	    return TCL_ERROR;
	}

	/* Retrieve fd */
        fd1 = get_fd_from_channel(channels[1], TCL_READABLE, TCL_WRITABLE);
        fd0 = get_fd_from_channel(channels[0], TCL_READABLE, TCL_WRITABLE);

        while ( (i=read(fd0, buffer, 1024)) > 0) {
            j=0; k=0;
            while (j<i) {
                /* retry the write if partial, but break on error */
                if ( (k=write(fd1,buffer+j,i-j)) < 0)
                    break;
                j+=k;
            }
	    if (fcopy_once) {
		break;
	    }
            if (k<0) {i=-2; break;}
        }
	if (fcopy_once && errno == EIO) i=0;
        if (i<0) {
            Tcl_AppendResult(interp, argv[0], ": ", argv[-i], ": ",
                             strerror(errno), NULL);
            return TCL_ERROR;
        }
	sprintf(string,"%i", i);
	Tcl_AppendResult(interp, string, NULL);
	return TCL_OK;

      /*
       * MP (14-06-99): added sys_chmod, sys_umask & sys_mknod
       *
       * See comments for usage.
       */

      case 'c':                                   /* sys_chmod */
      {
 	  /*
	   * sys_chmod
	   *
	   * Usage:
	   *   sys_chmod pathname mode
	   *   sys_chmod mode pathname [...]     (ARub, Nov 2001)
	   *
	   *  mode      - mode for file (use 0xxx for octal)
	   */

	  int mode;
	  char *file;

	  if (argc < 3) goto wna;
	  if (Tcl_GetInt(interp, argv[1], &mode) == TCL_OK) {
	      /* new way */
	      for (i=2; i<argc; i++) {
		  if (chmod(argv[i], (mode_t)mode) != 0) {
		      Tcl_AppendResult(interp, "sys_chmod(",
				       argv[i], "): ", strerror(errno),
				       NULL);
		      return TCL_ERROR;
		  }
	      }
	      return TCL_OK;
	  }
	  /* old way */
	  if (Tcl_GetInt(interp, argv[2], &mode) == TCL_OK) {
	      if (chmod(argv[1], (mode_t)mode) != 0) {
		  Tcl_AppendResult(interp, "sys_chmod: ", strerror(errno),
				   NULL);
		  return TCL_ERROR;
	      }
	      return TCL_OK;
	  }
	  /* recreate the error with argv[1] */
	  Tcl_GetInt(interp, argv[1], &mode);
	  Tcl_AppendResult(interp, "\nwhile parsing a file mode",
			   NULL);
	  return TCL_ERROR;
      }
      break;

      case 'u':                                   /* sys_umask */
      {
 	  /*
	   * sys_umask
	   *
	   * Usage:
	   *   sys_umask mask
	   *
	   *  mask      - umask (use 0xxx for octal)
	   *
	   * RETURN:
	   *  Previous octal umask value
	   */

	  int    mask;
	  mode_t prev;

	  if (argc != 2) goto wna;
	  if (Tcl_GetInt(interp, argv[1], &mask) != TCL_OK) {
	      Tcl_AppendResult(interp, "\nwhile parsing file creation mask",
			       NULL);
	      return TCL_ERROR;
	  }

	  prev = umask((mode_t)mask);
	  sprintf(interp->result, "%o", (unsigned int)prev );
	  return TCL_OK;
      }
      break;

      case 'm':                                   /* sys_mknod */
      {
	  /*
	   * sys_mknod
	   *
	   * Usage:
	   *   sys_mknod file_type pathname mode [major minor]
	   *
	   *  file_type - one of 'S_IFREG', 'S_IFCHR' or 'c' or 'u',
	   *                'S_IFBLK' or 'b', S_IFIFO or 'p'
	   *  pathname  - quite obvious
	   *  mode      - octal mode for new file
	   *  major     - major number for new device special file
	   *  minor     - minor number for new device special file
	   *
	   * major & minor are only valid (& necessary) if file_type is
	   * S_IFCHR or S_IFBLK
	   *
	   * minor numbers are restricted to range 0-255
	   */

	  mode_t ftype;
	  dev_t  dev = 0;
	  int mode;
	  int major, minor;

	  if (argc < 4) goto wna;
	  ftype = 0;
	  if (strcasecmp(argv[1], "S_IFREG") == 0)
	      ftype = S_IFREG;
	  else if ((strcasecmp(argv[1], "S_IFCHR") == 0) ||
		   (strcasecmp(argv[1], "c") == 0) ||
		   (strcasecmp(argv[1], "u") == 0))
	      ftype = S_IFCHR;
	  else if ((strcasecmp(argv[1], "S_IFBLK") == 0) ||
		   (strcasecmp(argv[1], "b") == 0))
	      ftype = S_IFBLK;
	  else if ((strcasecmp(argv[1], "S_IFIFO") == 0) ||
		   (strcasecmp(argv[1], "p") == 0))
	      ftype = S_IFIFO;

	  if ((ftype == S_IFCHR) || (ftype == S_IFBLK))
	  {
	      if (argc != 6) goto wna;
	  } else
	  {
	      if (argc != 4) goto wna;
	  }
	  if (Tcl_GetInt(interp, argv[3], &mode) != TCL_OK) {
	      Tcl_AppendResult(interp, "\nwhile parsing file mode",
			       NULL);
	      return TCL_ERROR;
	  }
	  if ((ftype == S_IFCHR) || (ftype == S_IFBLK))
	  {
	      if (Tcl_GetInt(interp, argv[4], &major) != TCL_OK) {
		  Tcl_AppendResult(interp, "\nwhile parsing major number",
				   NULL);
		  return TCL_ERROR;
	      }
	      if ((major < 0) || (major > 255))
	      {
		  Tcl_AppendResult(interp, "\nmajor number out of range",
				   NULL);
		  return TCL_ERROR;
	      }
	      if (Tcl_GetInt(interp, argv[5], &minor) != TCL_OK) {
		  Tcl_AppendResult(interp, "\nwhile parsing minor number",
				   NULL);
		  return TCL_ERROR;
	      }
	      if ((minor < 0) || (minor > 255))
	      {
		  Tcl_AppendResult(interp, "\nminor number out of range",
				   NULL);
		  return TCL_ERROR;
	      }

	      dev  = (major << 8);
	      dev |= minor;
	  }

	  mode |= ftype;

	  if (mknod(argv[2], mode, dev) != 0) {
	      Tcl_AppendResult(interp, "sys_mknod: ", strerror(errno),
			       NULL);
	      return TCL_ERROR;
	  }
	  return TCL_OK;
      }
      break;

      default:
        Tcl_SetResult(interp, "Unknown command", TCL_STATIC);
        return TCL_ERROR;
    }

  wna:
    Tcl_AppendResult(interp, argv[0], ": wrong # of args\n",
                     NULL);
    return TCL_ERROR;
}


#endif /* USE_PROSA_EXT */
