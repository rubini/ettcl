/* Prosa network command addons to tclsh for ET-Linux
 *
 * Copyright (c) 1998,1999    Alessandro Rubini (rubini@prosa.it)
 * Copyright (c) 1998,1999    Prosa Srl (prosa@prosa.it)
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
/* #include <type.h> */
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <net/route.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>

#include "ettclInt.h"
#include "ettclPort.h"

#ifdef TCL_USE_PROSA_EXT

/* Network commands of general utility */

static struct ifreq req;
static struct rtentry rte;

struct ifconfigtable {
    char *name;
    int ioctlname;
    struct sockaddr *where;
};

struct ifconfigtable param_table[] = {
     { "addr", SIOCSIFADDR, &req.ifr_addr },
     { "netmask", SIOCSIFNETMASK, &req.ifr_netmask },
     { "broadcast", SIOCSIFBRDADDR, &req.ifr_broadaddr },
     { "pointopoint", SIOCSIFDSTADDR, &req.ifr_dstaddr },
     { NULL, 0, NULL }
};
    

int Tcl_ProsaNetCmd(ClientData dummy, Tcl_Interp *interp, 
		    int argc, char **argv)
{
    static char string[128];
    int i, flags;
    static int fd=-1;

    if (fd<0) { /* Aargh! I just keep it open forever.... */
	if ((fd=socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
	    Tcl_AppendResult(interp, argv[0],": socket(): ",
			     strerror(errno), NULL);
	    return TCL_ERROR;
	}
    }
 
    switch(argv[0][0]) {
      case 'i':                                         /* ifconfig */
        if (argc==1 || argc>4) goto wna;

	strncpy(req.ifr_name,argv[1],IFNAMSIZ-1); /* ifname */
	if(ioctl(fd, SIOCGIFFLAGS, &req)<0) goto ioctlerr;
	flags = req.ifr_flags;

	if (argc==2) { /* report about one interface */
	    sprintf(string,"%s: %s", req.ifr_name,
		    flags&IFF_UP ? "up" : "down");
	    Tcl_AppendResult(interp, string, NULL);

	    if (ioctl(fd, SIOCGIFADDR, &req)<0) goto ioctlerr;
	    sprintf(string," addr %s ",
		    inet_ntoa((*(struct sockaddr_in *)
			       &req.ifr_addr).sin_addr));
	    Tcl_AppendResult(interp, string, NULL);

	    if (ioctl(fd, SIOCGIFNETMASK, &req)<0) goto ioctlerr;
	    sprintf(string," mask %s ",
		    inet_ntoa((*(struct sockaddr_in *)
			       &req.ifr_netmask).sin_addr));
	    Tcl_AppendResult(interp, string, NULL);

	    if (ioctl(fd, SIOCGIFBRDADDR, &req)<0) goto ioctlerr;
	    sprintf(string," bcast %s\n",
		    inet_ntoa((*(struct sockaddr_in *)
			       &req.ifr_broadaddr).sin_addr));
	    Tcl_AppendResult(interp, string, NULL);

	    if (ioctl(fd, SIOCGIFDSTADDR, &req)<0) goto ioctlerr;
	    sprintf(string,"\tpointopoint %s ",
		    inet_ntoa((*(struct sockaddr_in *)
			       &req.ifr_addr).sin_addr));
	    Tcl_AppendResult(interp, string, NULL);

	    if (ioctl(fd, SIOCGIFHWADDR, &req)<0) goto ioctlerr;
	    sprintf(string," hwaddr %02x:%02x:%02x:%02x:%02x:%02x ",
		    req.ifr_hwaddr.sa_data[0] & 0xff,
		    req.ifr_hwaddr.sa_data[1] & 0xff,
		    req.ifr_hwaddr.sa_data[2] & 0xff,
		    req.ifr_hwaddr.sa_data[3] & 0xff,
		    req.ifr_hwaddr.sa_data[4] & 0xff,
		    req.ifr_hwaddr.sa_data[5] & 0xff);
	    Tcl_AppendResult(interp, string, NULL);

	    if (ioctl(fd, SIOCGIFMTU, &req)<0) goto ioctlerr;
	    sprintf(string," mtu %i ",(int) req.ifr_mtu);
	    Tcl_AppendResult(interp, string, NULL);

	    return TCL_OK;
	}

	if (argc==3) { /* up or down */
	    i=0; /* number of successfull operations */
	    if (!strcmp(argv[2],"up")) {
		i++; flags |= IFF_UP;
	    }
	    if (!strcmp(argv[2],"down")) {
		i++; flags &= ~IFF_UP;
	    }
	    if (i) {
		req.ifr_flags = flags;
		if (ioctl(fd, SIOCSIFFLAGS, &req)<0) goto ioctlerr;
		return TCL_OK;
	    }
	}
	if (argc==4) {
	    struct in_addr addr;

	    if (!inet_aton(argv[3], &addr)) goto wna;

	    for (i=0; param_table[i].name; i++) {
		if (!strcmp(param_table[i].name, argv[2])) {
		    (*(struct sockaddr_in *)(param_table[i].where))
			.sin_addr = addr;
		    param_table[i].where->sa_family = AF_INET;
		    if (ioctl(fd, param_table[i].ioctlname, &req))
			goto ioctlerr;
		    return TCL_OK;
		}
	    }
	}

        goto wna;

      case 'r':                                         /* route */
        if (argc==1) { /* report config */
	    goto wna;
	}

	memset(&rte, 0, sizeof(rte));
	rte.rt_dst.sa_family = AF_INET;
	rte.rt_genmask.sa_family = AF_INET;
	rte.rt_gateway.sa_family = AF_INET;

	if (!strcmp(argv[1],"add")) {
	    /*
	     * This is *way* incomplete.
	     * Accepts only
	     *              route add default gw <gw> dev <dev>
	     *              route add <addr> netmask <mask> dev <dev>
	     */
	    if (argc!=7)
		goto wna;
	    i=0;
	    if (!strcmp(argv[2],"default")) {
		if (strcmp(argv[5],"dev") || strcmp(argv[3],"gw"))
		    goto wna;
		i++; /* valid */
		if (!inet_aton(argv[4],
			&((*(struct sockaddr_in *)&rte.rt_gateway).sin_addr)))
		    goto wna;
		rte.rt_flags = RTF_UP | RTF_GATEWAY;
		rte.rt_metric = 1;
		rte.rt_dev = argv[6];
		
	    }
	    if (i==0) {
		if (strcmp(argv[3],"netmask") || strcmp(argv[5],"dev"))
		    goto wna;
		i++; /* valid */
		if (!inet_aton(argv[2],
			&((*(struct sockaddr_in *)&rte.rt_dst).sin_addr)))
		    goto wna;
		if (!inet_aton(argv[4],
			&((*(struct sockaddr_in *)&rte.rt_genmask).sin_addr)))
		    goto wna;
		rte.rt_flags = RTF_UP;
		rte.rt_metric = 0;
		rte.rt_dev = argv[6];
	    }

	    if (i==0) goto wna;

	    if (ioctl(fd, SIOCADDRT, &rte))
		goto ioctlerr;
	    return TCL_OK;

	}
	
	goto wna; /* still unimplemented */

    
      default:
        Tcl_SetResult(interp, "Unknown command", TCL_STATIC);
        return TCL_ERROR;
    }

  wna:
    Tcl_AppendResult(interp, argv[0], ": Wrong number of arguments ",
                     "or syntax error",NULL);
    return TCL_ERROR;

  ioctlerr:
    Tcl_AppendResult(interp, argv[0],": ioctl(): ",
		     strerror(errno), NULL);
    return TCL_ERROR;

}

/* ======================================================= Udp support */

/*
 * if configured to, we can copy any outgoing udp packet to a
 * specific IP:port pair, for monitoring purposes
 */
static int udp_monitor_socket = -1;

/*================================================ low-level */

static int udp_base_client_port = 60000; /* range where clients are allocated */
static int udp_max_port         = 62000;

static int udp_random_port(void)
{
    static int done=0;

    if (!done) {
	srand((unsigned int) time(NULL));
	done++;
    }
    return udp_base_client_port + rand() % (udp_max_port-udp_base_client_port);
}

static int udp_bind(int sock, int port)
{
    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;
    return bind(sock, (struct sockaddr *)&addr, sizeof(addr));
}

/* parse an "IP:port" pair and tell how many fields are there */    
static int udp_str_to_addr(Tcl_Interp *interp, char *s,
			   struct sockaddr_in *addr, char *msghead)
{
    char *t = strdup(s);
    char *p;
    int port, ipok=0;
    int retval = 2; /* both IP and port */

    if (!t) return 0; /* Hmm... */

    if ( (p=strchr(t,':')) ) { /* a port is there too */
	if (Tcl_GetInt(interp, p+1, &port)!=TCL_OK) {
	    free(t); return 0; /* not converted */
	}
	*p = '\0'; /* cut the IP address */
	addr->sin_port = htons(port);
    }
    else retval = 1; /* no port is there */

    if (inet_aton(t, &addr->sin_addr))
	ipok = 1;
    if (!ipok) {
	/* try with DNS */
	struct hostent *host = gethostbyname(t);
        if (host) {
	    addr->sin_addr.s_addr = *(unsigned int *)host->h_addr;
	    ipok = 1;
	}
	else
	    Tcl_AppendResult(interp, msghead, "invalid hostname: ", t, NULL);
    }
    free(t);
    if (!ipok) return 0; /* failed */
    addr->sin_family = AF_INET;
    return retval;
}

/* keep a list of open sockets, to avoid always using the Tcl stuff */
struct udp_item {
    int fd;
    char *localname;
    struct sockaddr_in local;
    char *remotename;
    struct sockaddr_in remote;
    Tcl_Channel channel;
    char *name;
    struct udp_item *next;
};

static struct udp_item *udp_head;
static struct udp_item *udp_lastused; /* cache */
static struct udp_item *udp_item_monitor; /* cache */

/* find a socket by name */
static struct udp_item *udp_find(char *name)
{
    struct udp_item *item;

    if (!udp_head) return NULL; /* nothing there */
    if (!strcmp(udp_lastused->name, name))
	return udp_lastused;
    for (item = udp_head; item; item = item->next)
	if (!strcmp(item->name,name))
	    return udp_lastused = item;
    return NULL;
}

/*================================================ mid-level */

/*
 * Socket options are used at open time. The are specified as "-o opt,opt".
 * It would be interesting to get a value as well ("-o opt=val,opt,opt=val"),
 * but valued options only make sense in TCP. It would be interesting to
 * have "binary" as an internal option (and flag), but that can't be done
 * as Tcl is text-based and nulls are end-of-string regardless
 *                              -- ARub Dec 2000
 */
struct sockopt {
    char *name; int value; 
} sockopt[] = {
    {"reuseaddr", SO_REUSEADDR},
    {"broadcast", SO_BROADCAST},
    {NULL, 0}
};

static int udp_open(Tcl_Interp *interp, int argc, char **argv)
{
    struct sockaddr_in saddr, raddr;
    int i, sock, port;
    Tcl_Channel channel;
    char *name;
    struct udp_item *item;

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
	Tcl_AppendResult(interp, "upd open: socket(): ",
			 strerror(errno), NULL);
	return TCL_ERROR;
    }

    /*
     * The first argument can be "-o" and then a list of options
     * can appear.
     */
    if (argc>2 && !strcmp(argv[1],"-o")) {
	struct sockopt *optr; int val=1;
	char *o, *no = argv[2]; /* netxt opt */
	while ( (o=no) ) {
	    no = strchr(o, ',');
	    if (no) {*no = '\0'; no++;}
	    /* look for o */
	    for (optr = sockopt; optr->name; optr++)
		if (!strcmp(optr->name, o))
		    break;
	    if (setsockopt(sock, SOL_SOCKET, optr->value,
			   &val, sizeof(int)) < 0) {
		Tcl_AppendResult(interp, "upd open: setsockopt(", optr->name,
				 "): ", strerror(errno), NULL);
		close(sock);
		return TCL_ERROR;
	    }
	} /* while */
	argc -= 2; argv += 2;/* eat both arguments */
    }

    if (argc<2 || argc>3) {
	Tcl_SetResult(interp, "upd open: wna", TCL_STATIC);
	close(sock);
	return TCL_ERROR;
    }

    memset(&saddr, 0, sizeof(saddr));
    if (!strcmp(argv[1], "client"))
	port = 0;
    else 
	if (Tcl_GetInt(interp, argv[1], &port)!=TCL_OK) {
	    Tcl_AppendResult(interp, "upd open: wrong port: ", argv[1], NULL);
	    close(sock);
	    return TCL_ERROR;
	}

    /*
     * ok, we have a socket and a port. Now bind
     */
    if (!port) {
	for (i=udp_random_port(); i<udp_max_port; i++)
	    if (!udp_bind(sock, i))
		break;
	port = i;
    } else
	if (udp_bind(sock, port))
	    port = udp_max_port; /* fail */

    /* FIXME: record the local address */

    if (port==udp_max_port) {
	Tcl_AppendResult(interp, "upd open: socket(): ",
			 strerror(errno), NULL);
	close(sock);
	return TCL_ERROR;
    }

    /*
     * If there is a third argument, use it
     */
    memset(&raddr, 0, sizeof(raddr));
    if (argc==3) {
	i = udp_str_to_addr(interp, argv[2],&raddr, "udp open: ");
	if (i != 2) {
	    if (i == 1) { /* if 0, a message is already there */
		Tcl_AppendResult(interp, "udp open: no port nr. in\"",
				 argv[2], "\"", NULL);
	    }
	    close(sock);
	    return TCL_ERROR;
	}
    }

    /*
     * Allocate your storage
     */
    if (!(item = calloc(1, sizeof(*item)))) {
	Tcl_AppendResult(interp, "upd open: malloc(): ",
			 strerror(errno), NULL);
	close(sock);
	return TCL_ERROR;
    }

    /*
     * create a Tcl channel
     */
    channel = Tcl_MakeFileChannel((ClientData)sock, (ClientData)sock,
				  TCL_READABLE | TCL_WRITABLE);
    Tcl_RegisterChannel(interp, channel);
    name = Tcl_GetChannelName(channel);

    /*
     * remember all of this stuff (FIXME: remembre localname too)
     */
    item->fd = sock;
    item->name = strdup(name);
    item->remote = raddr;
    if (raddr.sin_addr.s_addr != INADDR_ANY) {
	item->remotename = strdup(inet_ntoa(raddr.sin_addr));
    }
    item->channel = channel;
    item->next = udp_head;
    udp_lastused = udp_head = item;

    /* done */
    Tcl_SetResult(interp, name, TCL_VOLATILE);
    return TCL_OK;
}

static int udp_close(Tcl_Interp *interp, int argc, char **argv)
{
    struct udp_item *item, **prev;

    if (argc != 2) {
	Tcl_SetResult(interp, "upd close: wna", TCL_STATIC);
	return TCL_ERROR;
    }
    /* don't use udp_find, as we need the prev ptr too */
    for (item = udp_head, prev = &udp_head; item;
	 prev = &item->next, item = item->next)
	if (!strcmp(item->name, argv[1]))
	    break;
    if (!item) {
	Tcl_AppendResult(interp, "upd close: no such socket \"",
			 argv[1], "\"", NULL);
	return TCL_ERROR;
    }
    if (udp_monitor_socket == item->fd) {
	udp_monitor_socket = -1;
	udp_item_monitor = NULL;
    }
    if (item->localname) free(item->localname);
    if (item->remotename) free(item->remotename);
    free(item->name);
    *prev = item->next;
    free(item);
    udp_lastused = udp_head; /* can't be invalid */
    return Tcl_VarEval(interp, "close ", argv[1], NULL); /* this calls close */
}

static int udp_sendto(Tcl_Interp *interp, int argc, char **argv)
{
    struct udp_item *item;
    struct sockaddr_in to;
    int len;

    if (argc<3 || argc>4) {
        Tcl_SetResult(interp, "upd sendto: wna", TCL_STATIC);
        return TCL_ERROR;
    }
    item = udp_find(argv[1]);
    if (!item) {
        Tcl_AppendResult(interp, "udp sendto: no such socket \"",
                         argv[1], "\"", NULL);
        return TCL_ERROR;
    }
    to = item->remote;
    if (argc==4) { /* sendto <sock> <to> <msg> */
	len = udp_str_to_addr(interp, argv[2], &to, "udp sendto: ");
	if (len != 2) {
	    if (len == 1) { /* if 0, a message is already there */
		Tcl_AppendResult(interp, "udp sendto: no port nr. in \"",
				 argv[2], "\"", NULL);
	    }
	    return TCL_ERROR;
	}
	argc--; argv++;
    }
    if (!(to.sin_family)) {
	Tcl_AppendResult(interp, "udp sendto: not default nor explicit dest.",
			 NULL);
	return TCL_ERROR;
    }

    /*
     * send a packet to the monitor, if defined
     */
    if (udp_item_monitor) {
	char buf[4096]; /* bad: fixed size */
	len = sprintf(buf, "T %5i mesg %2i %s:%i %s:%i \"%s\"\n",
		      getpid(),
		      udp_item_monitor->fd,
		      udp_item_monitor->localname,
		      ntohs(udp_item_monitor->local.sin_port), 
		      udp_item_monitor->remotename 
		         ?:  inet_ntoa(udp_item_monitor->remote.sin_addr),
		      ntohs(udp_item_monitor->remote.sin_port),
		      argv[2]);
	sendto(udp_monitor_socket, buf, len, 0,
	       (struct sockaddr *)&udp_item_monitor->remote,
	       sizeof(udp_item_monitor->remote));
    }

    /* now "sendto <sock> <msg>" */
    if (sendto(item->fd, argv[2], strlen(argv[2]), 0,
	       (struct sockaddr *)&to, sizeof(to))<0) {
	Tcl_AppendResult(interp, "udp sendto: sendto(): ",
                         strerror(errno), NULL);
        return TCL_ERROR;
    }
    return TCL_OK;

}

static int udp_recvfrom(Tcl_Interp *interp, int argc, char **argv)
{
    struct udp_item *item;
    struct sockaddr_in from;
    unsigned char *buf;
    unsigned char name[24];
    int len, addrlen;

    if (argc!=3) {
        Tcl_SetResult(interp, "upd recvfrom: wna", TCL_STATIC);
        return TCL_ERROR;
    }
    item = udp_find(argv[1]);
    if (!item) {
        Tcl_AppendResult(interp, "udp recvfrom: no such socket \"",
                         argv[1], "\"", NULL);
        return TCL_ERROR;
    }
    buf = malloc(10240);
    if (!buf) {
        Tcl_AppendResult(interp, "udp recvfrom: ", strerror(errno), NULL);
        return TCL_ERROR;
    }
    addrlen = sizeof(from);
    len = recvfrom(item->fd, buf, 10240, 0,
		   (struct sockaddr *)&from, &addrlen);
    if (len <0) {
	free(buf);
	Tcl_AppendResult(interp, "udp recvfrom: recvfrom(): ",
                         strerror(errno), NULL);
        return TCL_ERROR;
    }
    buf[len]='\0';
    if (buf[len-1]=='\n') /* dirty :) */
	buf[--len]='\0';

    sprintf(name,"%s:%i", inet_ntoa(from.sin_addr), ntohs(from.sin_port));
    Tcl_SetVar(interp, argv[2], name, 0);
    Tcl_SetResult(interp, buf, TCL_VOLATILE);
    free(buf);
    return TCL_OK;
}

static int udp_join(Tcl_Interp *interp, int argc, char **argv)
{
    struct udp_item *item;
    struct ip_mreq mreq;
    int sock;

    if (argc != 3) {
        Tcl_SetResult(interp, "upd join: wna", TCL_STATIC);
        return TCL_ERROR;
    }
    item = udp_find(argv[1]);
    if (!item) {
	Tcl_AppendResult(interp, "udp join: no such socket \"",
			 argv[1], "\"", NULL);
	return TCL_ERROR;
    }
    sock = item->fd;
    
    if (inet_aton(argv[2], &mreq.imr_multiaddr)==0) {
	Tcl_AppendResult(interp, "udp join: invalid address \"",
			 argv[2], "\"", NULL);
	return TCL_ERROR;
    }
    mreq.imr_interface.s_addr = INADDR_ANY;
    if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP,
		   &mreq, sizeof(mreq)) < 0) {
	Tcl_AppendResult(interp, "udp join: ioclt(): ", strerror(errno), NULL);
	return TCL_ERROR;
    }
    return TCL_OK;
}

static int udp_monitor(Tcl_Interp *interp, int argc, char **argv)
{
    struct udp_item *item;

    if (argc != 2) {
        Tcl_SetResult(interp, "upd setmonitor: wna", TCL_STATIC);
        return TCL_ERROR;
    }
    item = udp_find(argv[1]);
    if (!item) {
	Tcl_AppendResult(interp, "udp setmonitor: no such socket \"",
			 argv[1], "\"", NULL);
	return TCL_ERROR;
    }
    udp_item_monitor = item;
    udp_monitor_socket = item->fd;
    return TCL_OK;
}

/*=============================================== high-level */

int Tcl_ProsaUdpCmd(ClientData unused, Tcl_Interp *interp,
                int argc, char **argv)
{

    struct {
	char *name;
	int (*f)(Tcl_Interp *interp, int ac, char **av);
    } subcmd[] = {
	{"open",     udp_open},     /* open <port> [<remote>:<rport>] */
	{"close",    udp_close},    /* close <sock>                   */
	{"sendto",   udp_sendto},   /* sendto <sock> [<to>] <msg>     */
	{"recvfrom", udp_recvfrom}, /* recvfrom <sock> <fromvar>      */
	{"setmonitor", udp_monitor},/* setmonitor <sock>              */
	{"join",     udp_join},     /* join <sock> <ipaddr>           */
	{NULL,       NULL}
    };
    int i;

    if (argc<2) {
	Tcl_SetResult(interp, "udp: too few args", TCL_STATIC);
	return TCL_ERROR;
    }

    for (i=0; subcmd[i].f; i++)
	if (!strcmp(subcmd[i].name, argv[1]))
	    break;
    
    if (!subcmd[i].f) {
	Tcl_SetResult(interp, "udp: undefined subcmd", TCL_STATIC);
	return TCL_ERROR;
    }
    return subcmd[i].f(interp, argc-1, argv+1);
}
















#endif /* TCL_USE_PROSA_EXT */





