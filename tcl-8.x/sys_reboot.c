/* 
 * sys_reboot.c --
 *
 *      Implement the "sys_reboot" command.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/reboot.h>

#include "ettcl.h"

/* **************************************************************************
 * Usage:
 *   sys_reboot cmd
 *
 *   cmd	- either "reboot" or "halt"
 * ************************************************************************** */
int ettcl_sys_reboot(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   if (objc < 2) {
      Tcl_WrongNumArgs(interp, 1, objv, "cmd");
      return TCL_ERROR;
   }

   /* If we are not init... no way, cannot reboot! */
   if (getpid() != 1) { /* Not init */
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": only init can do that", NULL);
      return TCL_ERROR;
   }

   if (strcmp(Tcl_GetString(objv[1]), "reboot") == 0) {
      sync();
      sleep(1);

#ifdef __GLIBC__
      reboot(RB_AUTOBOOT);
#else
      reboot(0xfee1dead, 672274793, 0x1234567);
#endif

      /* If we are here we got an error! */
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": ", strerror(errno), NULL);
      return TCL_ERROR;
   } 
   else if (strcmp(Tcl_GetString(objv[1]), "halt") == 0) {
      kill(SIGTERM, -1);
      kill(SIGKILL, -1);

      sync();
      sleep(1);
 
      /* On reboot we simply end here... */
      exit(0);
   }

   Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                            ": unknow subcommand", NULL);
   return TCL_ERROR;
}
