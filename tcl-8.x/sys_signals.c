/* 
 * sys_signals.c --
 *
 *      Implement the "sys_signal" and "sys_kill" commands.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/types.h>

#include "ettcl.h"

/* **************************************************************************
 * Global variables
 * ************************************************************************** */

struct signal_data signal_data[NSIG];

/* **************************************************************************
 * Private functions
 * ************************************************************************** */

/* This procedure is called for each registered signal by using `signal' */
static void signal_handler(int signo)
{
   /* We simply mark the TCL signal handler and get the process PID */
   signal_data[signo].pid = getpid();
   Tcl_AsyncMark(signal_data[signo].token);
}

/* This procedure is the TCL signal handler */
int async_proc(ClientData clientData, Tcl_Interp *cmd_interp, int cmd_code)
{
   int signo = (int) clientData;
   struct signal_data *sd = &signal_data[signo];
   Tcl_Interp *interp = sd->interp;
   Tcl_Obj *cmd = sd->command;
   Tcl_Obj *signal_error_info, *cmd_info;
   int hnd_code;

   /* Skip if the signal was not for us */
   if (signal_data[signo].pid != getpid())
      return TCL_OK;

   /* Set the `signal_no' variable */
   Tcl_UnsetVar2(interp, "tcl_signal", NULL, TCL_GLOBAL_ONLY);
   Tcl_SetVar2Ex(interp, "tcl_signal", NULL,
                         Tcl_NewStringObj(signal_num2name(signo), -1),
                         TCL_GLOBAL_ONLY);

   /* Save the command's return value */
   cmd_info = Tcl_GetObjResult(interp);
   Tcl_IncrRefCount(cmd_info);

   /* Execute the command and get the error code and value */
   Tcl_ResetResult(interp);   /* Clear the result for interp */
   hnd_code = Tcl_EvalObjEx(interp, cmd, TCL_EVAL_GLOBAL);
   signal_error_info = Tcl_GetObjResult(interp);
   Tcl_IncrRefCount(signal_error_info);

   /* Reset the previous result since we always return it as return value */
   Tcl_SetObjResult(interp, cmd_info);
   Tcl_DecrRefCount(cmd_info);

   /* Unset the `signal_no' variable */
   Tcl_UnsetVar(interp, "signal_no", TCL_GLOBAL_ONLY);

   /* Return the "signal_error" */
   if (hnd_code == TCL_OK)
      Tcl_SetVar(interp, "tcl_signal_error", "false", TCL_GLOBAL_ONLY);
   else {
      Tcl_SetVar(interp, "tcl_signal_error", "true", TCL_GLOBAL_ONLY);

      /* Return the "signal_error_info" */
      Tcl_UnsetVar2(interp, "tcl_signal_error_info", NULL, TCL_GLOBAL_ONLY);
      Tcl_SetVar2Ex(interp, "tcl_signal_error_info", NULL,
                            signal_error_info, TCL_GLOBAL_ONLY);
   }
   Tcl_DecrRefCount(signal_error_info);

   /* Return the interrupted command return code (if exist) */
   if (cmd_interp == NULL) {
      /* We need restore handler info error message */
      Tcl_SetObjResult(interp, signal_error_info);
      return hnd_code;
   }
   return cmd_code;
}

/* **************************************************************************
 * Usage:
 *   sys_signal signo ?handler?
 *
 *   signo	- signal number or name (ex. CHLD)
 *   handler	- a TCL command or DEFAULT or IGNORE
 * ************************************************************************** */
int ettcl_sys_signal(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   int signo;
   struct sigaction act;

   if (objc < 2) {
      Tcl_WrongNumArgs(interp, 1, objv, "signo ?handler?");
      return TCL_ERROR;
   }

   /* Ok, check the signal name */
   if ((signo = signal_name2num(interp, Tcl_GetString(objv[1]))) < 0) {
       Tcl_AppendResult(interp, " while parsing signo", NULL);
       return TCL_ERROR;
   }

   /* In any case we return the previous signal handler */
   Tcl_SetObjResult(interp, signal_data[signo].command);
   Tcl_DecrRefCount(signal_data[signo].command);

   /* Check if a signal handler's body has been supplied */
   if (objc > 2) {
      /* Ok, we are going to install new signal handler! */
   
      /* Set new signal handler's body */
      signal_data[signo].command = objv[2];
      Tcl_IncrRefCount(signal_data[signo].command);

      /* Save the current interp pointer */
      signal_data[signo].interp = interp;
   
      /* Some settings... */
      sigemptyset(&act.sa_mask);
      act.sa_flags = SA_RESTART;   /* needed by TCL main loop */
   
      /* Check for a normal signal handler or for "special" one */
      if (!strcmp(Tcl_GetString(objv[2]), "DEFAULT"))
         act.sa_handler = SIG_DFL;
      else if (!strcmp(Tcl_GetString(objv[2]), "IGNORE"))
         act.sa_handler = SIG_IGN;
      else
         act.sa_handler = signal_handler;
   
      /* Install the signal handler */
      if (sigaction(signo, &act, NULL) < 0) {
         /* Restore previous signal handler's body.

            Doing this we doesn't activate the signal handler
            but we avoid losing its body! */
         Tcl_DecrRefCount(signal_data[signo].command);
         signal_data[signo].command = Tcl_GetObjResult(interp);
         Tcl_IncrRefCount(signal_data[signo].command);

         /* Set error message */
         Tcl_ResetResult(interp);
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                  ": ", strerror(errno), NULL);
         return TCL_ERROR;
      }
   }

   return TCL_OK;
}

/* **************************************************************************
 * Usage:
 *   sys_kill ?signo? pids
 *
 *   signo	- signal number or name (ex. CHLD), per default is TERM
 *   pids 	- one or more process' PIDs as list
 * ************************************************************************** */
int ettcl_sys_kill(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   int i = 1, j;
   int signo = SIGTERM;   /* the default */
   int lobjc;
   Tcl_Obj **lobjv;
   pid_t pid;

   if (objc < 2) {
      Tcl_WrongNumArgs(interp, 1, objv, "?signo? pids");
      return TCL_ERROR;
   }

   /* Decode the signal name if supplied */
   if (objc > 2) {
      if ((signo = signal_name2num(interp, Tcl_GetString(objv[1]))) < 0) {
         Tcl_AppendResult(interp, " while parsing signo", NULL);
         return TCL_ERROR;
      }
      i = 2;   /* point to PIDs' list */
   }

   /* Get each PID into the list */
   if (Tcl_ListObjGetElements(interp, objv[i], &lobjc, &lobjv) != TCL_OK)
      return TCL_ERROR;
   for (j = 0; j < lobjc; j++) {
      if (Tcl_GetIntFromObj(interp, lobjv[j], &pid) != TCL_OK) {
         Tcl_AppendResult(interp, " while parsing pid", NULL);
         return TCL_ERROR;
      }

      if (kill(pid, signo) < 0) {
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                  ": ", strerror(errno), NULL);
         return TCL_ERROR;
      }
   }

   return TCL_OK;
}
