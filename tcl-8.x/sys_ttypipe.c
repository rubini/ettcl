/* 
 * sys_ttypipe.c --
 *
 *      Implement the "sys_ttypair" and "sys_pipe" commands.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 1998,1999         Prosa Srl <prosa@prosa.it>
 * Copyright (c) 1999              Marco Pantaleoni <panta@prosa.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ettcl.h"

/* **************************************************************************
 * Open a tty master and a tty slave
 * ************************************************************************** */
static int ttypair(int fds[2]) {
   char devname[] = "/dev/pty--";
   int i,j;
   char s1[] = "pqrst";
   char s2[] = "0123456789abcdef";

   /* Look for a free master tty */
   fds[0] = -1;
   for (i = 0; fds[0] < 0 && i < 5; i++)
      for (j = 0; fds[0] < 0 && j < 16; j++) {
         devname[8] = s1[i];
         devname[9] = s2[j];

         if ((fds[0] = open(devname, O_RDWR)) < 0) {
            if (errno == EIO) continue;
            i=5; break; /* break twice */
         }
      }
   if (fds[0] < 0)
       return -1;

   devname[5] = 't';   /* slave */
   if ((fds[1] = open(devname, O_RDWR)) < 0) {
      close(fds[0]);
      return -1;
   }

   return 0;
}

/* **************************************************************************
 * Usage:
 *   sys_pipe ?in out?
 *   sys_ttypair ?master slave?
 *
 *
 *   in		- var where to store the pipe reading channel
 *   out 	- var where to store the pipe writing channel
 *   master     - the tty master
 *   slave      - the tty slave
 * ************************************************************************** */
int ettcl_sys_ttypipe(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   char *cmd = Tcl_GetString(objv[0]);
   Tcl_Channel ch[2];
   int p[2];
   Tcl_Obj *list;

   if (objc == 2) {
      if (cmd[0] == 'p')   /* pipe */
         Tcl_WrongNumArgs(interp, 1, objv, "?in out?");
      else   /* ttypair */
         Tcl_WrongNumArgs(interp, 1, objv, "?master slave?");
      return TCL_ERROR;
   }

   if (cmd[0] == 'p') {   /* pipe */
      /* Create the pipe channel */
      if (pipe(p) < 0) {
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                  ": ", strerror(errno), NULL);
         return TCL_ERROR;
      }
   }
   else {   /* ttypair */
      /* Create the ttypair channel */
      if (ttypair(p) < 0) {
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                  ": ", strerror(errno), NULL);
         return TCL_ERROR;
      }
   }

   /* Create the extra stuff used internally by Tcl */
   if (cmd[0] == 'p') {   /* pipe */
      ch[0] = Tcl_MakeFileChannel((ClientData) p[0], TCL_READABLE);
      ch[1] = Tcl_MakeFileChannel((ClientData) p[1], TCL_WRITABLE);
   }
   else {   /* ttypair */
      ch[0] = Tcl_MakeFileChannel((ClientData) p[0], TCL_READABLE|TCL_WRITABLE);
      ch[1] = Tcl_MakeFileChannel((ClientData) p[1], TCL_READABLE|TCL_WRITABLE);
   }

   if (ch[0] == NULL || ch[1] == NULL) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": cannot create new streams", NULL);
      close(p[0]);
      close(p[1]);
      return TCL_ERROR;
   }
   Tcl_RegisterChannel(interp, ch[0]);
   Tcl_RegisterChannel(interp, ch[1]);

   if (objc == 3) {
      /* Store the result into user's variables */
      Tcl_SetVar(interp, Tcl_GetString(objv[1]), Tcl_GetChannelName(ch[0]), 0);
      Tcl_SetVar(interp, Tcl_GetString(objv[2]), Tcl_GetChannelName(ch[1]), 0);
   }
   else {
      /* build the result as a list */
      list = Tcl_NewListObj(0, NULL);
      Tcl_ListObjAppendElement(interp, list,
                               Tcl_NewStringObj(Tcl_GetChannelName(ch[0]), -1));
      Tcl_ListObjAppendElement(interp, list,
                               Tcl_NewStringObj(Tcl_GetChannelName(ch[1]), -1));
      Tcl_SetObjResult(interp, list);
   }

   return TCL_OK;
}
