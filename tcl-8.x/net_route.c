/* 
 * sys_route.c --
 *
 *      Implement the "route" command.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <net/route.h>
#include <unistd.h>

#include "ettcl.h"

/* **************************************************************************
 * "add" subcommand
 *
 * Usage:
 *   route add addr netmask mask dev name
 *   route add default gw addr dev name
 *
 *   addr	- an host address
 *   mask	- a network mask
 *   name	- a device name
 * ************************************************************************** */

static int route_add(int fd, ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   static struct rtentry rte;
 
   if (strcmp(Tcl_GetString(objv[1]), "default") == 0) {
      if (objc < 7 ||
          strcmp(Tcl_GetString(objv[3]), "dev") ||
          strcmp(Tcl_GetString(objv[5]), "gw")) {
         Tcl_WrongNumArgs(interp, 2, objv, "default gw addr dev name");
         return TCL_ERROR;
      }

      if (!inet_aton(Tcl_GetString(objv[4]),
                     &((*(struct sockaddr_in *) &rte.rt_gateway).sin_addr))) {
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]), ": ", 
                                  Tcl_GetString(objv[1]), 
                                  ": invalid addr", NULL);
         return TCL_ERROR;
      }

      rte.rt_flags = RTF_UP|RTF_GATEWAY;
      rte.rt_metric = 1;
      rte.rt_dev = Tcl_GetString(objv[6]);
   }
   else {
      if (objc < 7 ||
          strcmp(Tcl_GetString(objv[3]), "netmask") ||
          strcmp(Tcl_GetString(objv[5]), "dev")) {
         Tcl_WrongNumArgs(interp, 2, objv, "addr netmask mask dev name");
         return TCL_ERROR;
      }

      if (!inet_aton(Tcl_GetString(objv[2]),
                     &((*(struct sockaddr_in *) &rte.rt_dst).sin_addr))) {
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]), ": ", 
                                  Tcl_GetString(objv[1]), 
                                  ": invalid addr", NULL);
         return TCL_ERROR;
      }

      if (!inet_aton(Tcl_GetString(objv[4]),
                     &((*(struct sockaddr_in *) &rte.rt_genmask).sin_addr))) {
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]), ": ", 
                                  Tcl_GetString(objv[1]), 
                                  ": invalid mask", NULL);
         return TCL_ERROR;
      }

      rte.rt_flags = RTF_UP;
      rte.rt_metric = 0;
      rte.rt_dev = Tcl_GetString(objv[6]);
   }

   if (ioctl(fd, SIOCADDRT, &rte)) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]), ": ",
                               Tcl_GetString(objv[1]), 
                               ": ", strerror(errno), NULL);
      return TCL_ERROR;
   }

   return TCL_OK;
}

/* **************************************************************************
 * Usage:
 *   route cmd ?args?
 *
 *   cmd	- special route subcommand
 *   args 	- arguments for specified subcommand
 * ************************************************************************** */
int ettcl_net_route(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   int i;
   CONST84 char *cmds[] = {
      "add", NULL
   };
   int fd;
   int ret = TCL_OK;

   if (objc < 2) {
      Tcl_WrongNumArgs(interp, 1, objv, "cmd ?args?");
      return TCL_ERROR;
   }

   /* Decode the command */
   if (Tcl_GetIndexFromObj(interp, objv[1], cmds, "subcommand", 0, &i) != TCL_OK)
      return TCL_ERROR;

   /* We need a raw socket */
   if ((fd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]), 
                               ": ", strerror(errno), NULL);
      return TCL_ERROR;
   }

   /* Do the job */
   switch (i) {
      case 0 : {   /* add */
         ret = route_add(fd, NULL, interp, objc, objv);
         break;
      }
   }

   close(fd);

   return ret;
}
