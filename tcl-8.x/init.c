/* 
 * init.c --
 *
 *      Provides several initialization functions.
 *
 * Copyright (c) 2003 Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <signal.h>

#include "ettcl.h"

/* **************************************************************************
 * The Ettcl initialization procedure
 * ************************************************************************** */

static void init_data(void)
{
   int i;

   /* Init the data struct for signals management */
   for (i = 0; i < NSIG; i++) {
      signal_data[i].command = Tcl_NewStringObj("DEFAULT", -1);
      Tcl_IncrRefCount(signal_data[i].command);
      signal_data[i].token = Tcl_AsyncCreate(async_proc, (ClientData) i);
   }
}

/* **************************************************************************
 * Ettcl initialization procedure
 * ************************************************************************** */

int Ettcl_Init(Tcl_Interp *interp)
{
   /* Init data structs */
   init_data();

   /* Add the system commands */
   Tcl_CreateObjCommand(interp, "sys_chmod", ettcl_sys_chmod, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_dup", ettcl_sys_dup, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_exec", ettcl_sys_exec, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_fork", ettcl_sys_fork, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_kill", ettcl_sys_kill, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_mknod", ettcl_sys_mknod, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_nice", ettcl_sys_nice, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_opentty", ettcl_sys_opentty, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_pipe", ettcl_sys_ttypipe, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_reboot", ettcl_sys_reboot, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_signal", ettcl_sys_signal, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_sync", ettcl_sys_sync, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_ttypair", ettcl_sys_ttypipe, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_umask", ettcl_sys_umask, NULL, NULL);
   Tcl_CreateObjCommand(interp, "sys_wait", ettcl_sys_wait, NULL, NULL);

   /* Add the "Prosa" commands */
   Tcl_CreateObjCommand(interp, "inp", ettcl_cmd_iofuncs, NULL, NULL);
   Tcl_CreateObjCommand(interp, "inw", ettcl_cmd_iofuncs, NULL, NULL);
   Tcl_CreateObjCommand(interp, "outp", ettcl_cmd_iofuncs, NULL, NULL);
   Tcl_CreateObjCommand(interp, "outw", ettcl_cmd_iofuncs, NULL, NULL);
   Tcl_CreateObjCommand(interp, "readb", ettcl_cmd_iofuncs, NULL, NULL);
   Tcl_CreateObjCommand(interp, "readw", ettcl_cmd_iofuncs, NULL, NULL);
   Tcl_CreateObjCommand(interp, "readl", ettcl_cmd_iofuncs, NULL, NULL);
   Tcl_CreateObjCommand(interp, "scheduler", ettcl_cmd_scheduler, NULL, NULL);
   Tcl_CreateObjCommand(interp, "tftp", ettcl_cmd_tftp, NULL, NULL);
   Tcl_CreateObjCommand(interp, "uudecode", ettcl_cmd_uudecode, NULL, NULL);
   Tcl_CreateObjCommand(interp, "uuencode", ettcl_cmd_uuencode, NULL, NULL);
   Tcl_CreateObjCommand(interp, "writeb", ettcl_cmd_iofuncs, NULL, NULL);
   Tcl_CreateObjCommand(interp, "writew", ettcl_cmd_iofuncs, NULL, NULL);
   Tcl_CreateObjCommand(interp, "writel", ettcl_cmd_iofuncs, NULL, NULL);

   /* Add the networking commands */
   Tcl_CreateObjCommand(interp, "ifconfig", ettcl_net_ifconfig, NULL, NULL);
   Tcl_CreateObjCommand(interp, "route", ettcl_net_route, NULL, NULL);

   /* Register the package */
   if (Tcl_PkgProvide(interp, "ettcl", "14.0") != TCL_OK)
      return TCL_ERROR;

   return TCL_OK;
}
