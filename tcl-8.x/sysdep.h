/*
 * sysdep.h --
 *
 *      Try to resolve incompatibility problems across different
 *      TCL versions.
 *
 * Copyright (c) 2003              Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/* Compiling error if no TCL 8.x */
#if TCL_MAJOR_VERSION != 8
#  error "This package needs TCL 8.x!"
#endif

/* --- Incompatibilities ---------------------------------------------------- */

/* No CONST84 definition prior TCL 8.4 */
#if TCL_MINOR_VERSION < 4
#  define CONST84   /* nothing to define... */
#endif
