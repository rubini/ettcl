/* 
 * sys_tftp.c --
 *
 *      Implement the "tftp" command.
 *
 * Copyright (c) 2003              Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>

#include "ettcl.h"

/* **************************************************************************
 * TFTP defines
 * ************************************************************************** */

struct tftp_s {
   unsigned short op;
   struct {
      unsigned short num;
      unsigned char data[512];
   } msg;
};

#define TFTP_RRQ             1
#define TFTP_WRQ             2
#define TFTP_DATA            3
#define TFTP_ACK             4
#define TFTP_ERROR           5

#define TFTP_OCTET           0
#define TFTP_NETASCII        1

#define TFTP_HEADER	4   /* 2 unsigned shorts */
#define TFTP_S_L        sizeof(struct tftp_s)
#define SA_S_L          sizeof(struct sockaddr_in)
#define SA              struct sockaddr

char *tftp_error_msg[] = {
   "Generic error occour",   /* Not defined, see error message (if any) */
   "File not found",
   "Access Violation",
   "Disk full or allocation exceeded",
   "Illegal TFTP operation",
   "Unknow transfer ID",
   "File already exist",
   "No suck user"
};
enum {
   NOT_DEFINED = 0,
   FILE_NOT_FOUND,
   ACCESS_VIOLATION,
   DISK_FULL,
   ILLEGAL_OPERATION,
   UNKNOW_ID,
   FILE_EXIST,
   NO_USER,
   ERR_VECT_DIM
};

/* Command line options */
int type, port, timeout, attemps;
char *fname;

/* **************************************************************************
 * Conversion functions
 * ************************************************************************** */

/* Convert a buffer into netascii format. Conversions are CR, LF -> LF
   and CR, NULL -> CR */
static int netascii_to_file(char *prev, char *buf, int n, FILE *st)
{
   int i;
   char c;

   for (i = 0; i < n; i++) {
      c = buf[i];

      if (*prev == '\n') {
         if (buf[i] == '\r')
            c = '\r';
         else if (buf[i] == '\0')
            c = '\0';
      }

      if (fputc(c, st) == EOF)
         return -1;
      *prev = c;
   }

   return 0;
}

/* Convert a file in netascii format into a buffer. Conversions are
   LF -> CR, LF and CR -> CR, NULL */
static int file_to_netascii(char *succ, char *buf, int n, FILE *st)
{
   int i = 0;
   int c;

   if (*succ == '\r' || *succ == '\0') {
      buf[i] = *succ;
      i++;
   }

   while (1) {
      if (i >= n || feof(st))
         break;

      if ((c = fgetc(st)) == EOF)
         return -1;

      if (c == '\r' || c == '\n')
            c = (c == '\n' ? '\0' : c);
            buf[i++] = '\n';
         if (i < n-1)
            buf[i++] = c;
         else 
            *succ = c;
   }

   return i;
}

/* **************************************************************************
 * TFTP misc functions
 * ************************************************************************** */

/* Check a TFTP packet if contains an error message and, if the case,
   return a object pointer who contains the error message otherwise
   return NULL */
static char *tftp_check_error(struct tftp_s *pkt)
{
   char *ptr;

   /* Check if we received a error message */
   if (ntohs(pkt->op) != TFTP_ERROR)
      return NULL;

   /* An error! Return it */
   if (ntohs(pkt->msg.num) == 0)
      ptr = pkt->msg.data;
   else {
      if (pkt->msg.num >= ERR_VECT_DIM)
         ptr = tftp_error_msg[ntohs(pkt->msg.num)];
      else
         ptr = "unknow error code!";
   }

   return ptr;
}

/* Build an error packet according to the value of "code".
   If "code" is 0 the we check the current value of "errno" */
static void tftp_make_error_pkt(int code, struct tftp_s *pkt, int *len)
{
   pkt->op = htons(TFTP_ERROR);
   *len = TFTP_HEADER;

   if (code != NOT_DEFINED)   /* or (code != 0) ? */
      pkt->msg.num = htons(code);
   else
      pkt->msg.num = htons(errno == EACCES ? ACCESS_VIOLATION :
                           errno == ENOSPC ? DISK_FULL :
                                             NOT_DEFINED);   /* force 0 */

   if (pkt->msg.num == htons(NOT_DEFINED)) {
      strcpy(pkt->msg.data, tftp_error_msg[NOT_DEFINED]);
      *len += strlen(tftp_error_msg[NOT_DEFINED]);
   }
}

/* Read data from a file and fit them in a tftp packet according to its type */
static int tftp_read_data(struct tftp_s *pkt, int n, FILE *st)
{
   int ret = 0;
   static char succ;

   succ = (pkt->msg.num == 1) ? 'g' : succ;

   if (type == TFTP_OCTET)
      ret = fread(pkt->msg.data, 1, 512, st);
   else   /* if no octet... then ascii!! */
      ret = file_to_netascii(&succ, pkt->msg.data, 512, st);

   return ret;
}

/* Write a tftp packet to a file stream according to its type */
static int tftp_write_data(struct tftp_s *pkt, int n, FILE *st)
{
   int ret = 0;
   static char prev;

   prev = (pkt->msg.num == 1) ? 'g' : prev;

   if (type == TFTP_OCTET)
      ret = (fwrite(pkt->msg.data, n, 1, st) < 1 ? -1 : 0);
   else   /* if no octet... then ascii!! */
      ret = netascii_to_file(&prev, pkt->msg.data, n, st);

   return ret;
}

/* Do a tftp accept */
static int tftp_accept(unsigned short *mode, FILE **st, struct tftp_s *pkt, int *pkt_l)
{
   int asock, sock;
   struct sockaddr_in aaddr, addr;
   int n = SA_S_L;
   char *fname;

   struct timeval to;
   fd_set rs;
   int i;

   bzero(&addr, SA_S_L);
   addr.sin_family = AF_INET;
   addr.sin_addr.s_addr = htonl(INADDR_ANY);
   addr.sin_port = htons((unsigned short) port);

   /* Get a socket */
   if ((asock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
      return -1;
   if (bind(asock, (SA *) &addr, SA_S_L) < 0) {
      close(asock);
      return -1;
   }

   /* Wait a request (no timeout here) */
   bzero(&aaddr, SA_S_L);
   if ((*pkt_l = recvfrom(asock, pkt, TFTP_S_L, 0, (SA *) &aaddr, &n)) < 0) {
      close(asock);
      return -1;
   }

   addr.sin_port = 0;
   close(asock);   /* not needed anymore */

   if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
      return -1; 

   if (connect(sock, (SA *) &aaddr, n) < 0) {
      close(sock);
      return -1;
   }

   /* Check the request */
   *mode = ntohs(pkt->op);
   fname = (char *) &pkt->msg;
   if (*((char *) (&pkt->msg)+strlen(fname)+1) == 'n')
      type = TFTP_NETASCII;

   /* Try opening local file in current directory for writing */
   *st = NULL;
   if (*mode != TFTP_RRQ && *mode != TFTP_WRQ)
      tftp_make_error_pkt(ILLEGAL_OPERATION, pkt, &n);
   else if ((*st = fopen(fname, (*mode == TFTP_RRQ ? "r" : "w"))) == NULL)
      tftp_make_error_pkt(NOT_DEFINED, pkt, &n);

   /* Make the reply */
   if (*st && *mode == TFTP_RRQ) {
      /* Make the data packet */
      pkt->op = htons(TFTP_DATA);
      pkt->msg.num = 1;
      if ((n = tftp_read_data(pkt, 512, *st)) < 0)
         tftp_make_error_pkt(NOT_DEFINED, pkt, &n);
   }
   else if (*st && *mode == TFTP_WRQ) {
      /* Make the ack packet */
      pkt->op = htons(TFTP_ACK);
      pkt->msg.num = htons(0);
      n = TFTP_HEADER;
   }

   for (i = 0; i < attemps; i++) {
      /* Send the reply */
      if (write(sock, pkt, n) < 0) {
         close(sock);
         if (*st)
            fclose(*st);
         return -1;
      }
   
      /* Wait the first packet if requested */
      if (*st) {
         FD_ZERO(&rs); FD_SET(sock, &rs);
         to.tv_sec = timeout; to.tv_usec = 0;
         if (select(sock+1, &rs, NULL, NULL, &to) < 0)
            return -1;
   
         if (!FD_ISSET(sock, &rs))   /* the time is over... */
            continue;
   
         if ((*pkt_l = read(sock, pkt, TFTP_S_L)) < 0) {
            close(sock);
            fclose(*st);
            return -1;
         }
         break;
      }
   }
   if (i >= attemps)
      return -1;

   return sock;
}

/* Do a tftp connect */
static int tftp_connect(char *h_addr, unsigned short mode, char *fname, struct tftp_s *pkt, int *pkt_l)
{
   int sock;
   struct sockaddr_in caddr, addr;
   int n = SA_S_L;

   struct tftp_s cpkt;

   struct timeval to;
   fd_set rs;
   int i;

   bzero(&addr, SA_S_L);
   addr.sin_family = AF_INET;
   memcpy(&addr.sin_addr, h_addr, sizeof(int));
   addr.sin_port = htons((unsigned short) port);

   /* Get a socket */
   if ((sock = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
      return -1;

   /* Do the request message */
   cpkt.op = htons(mode);
   sprintf((char *) &cpkt.msg, "%s%c%s",
           fname, '\0',
           type == TFTP_OCTET ? "octet" : "netascii");

   for (i = 0; i < attemps; i++) {
      /* Send the request to the server and wait for an answer */
      if (sendto(sock, &cpkt, TFTP_S_L, 0, (SA *) &addr, SA_S_L) < 0) {
         close(sock);
         return -1;
      }

      FD_ZERO(&rs); FD_SET(sock, &rs);
      to.tv_sec = timeout; to.tv_usec = 0;
      if (select(sock+1, &rs, NULL, NULL, &to) < 0)
         return -1;

      if (!FD_ISSET(sock, &rs))   /* the time is over... */
         continue;

      if ((*pkt_l = recvfrom(sock, pkt, TFTP_S_L, 0, (SA *) &caddr, &n)) < 0) {
         close(sock);
         return -1;
      }
      break;
   }
   if (i >= attemps)
      return -1;

   /* Connection ok! Change the port number and connect the socket */
   addr.sin_port = caddr.sin_port;
   if (connect(sock, (SA *) &addr, SA_S_L) < 0) {
      close(sock);
      return -1;
   }

   return sock;
}

static int tftp_recv_file(Tcl_Interp *interp, int sock, struct tftp_s *pkt, int pkt_l, FILE *st)
{
   int flag, err;
   unsigned short bnum = 1;
   char *str;

   struct tftp_s spkt;

   struct timeval to;
   fd_set rs;
   int i, n;

   /* The first data packet should be already received into connect/accept */

   for (; 1; bnum++) {
      /* Check if we received a data message */
      if (ntohs(pkt->op) != TFTP_DATA) {
         str = tftp_check_error(pkt);
         Tcl_AppendResult(interp, str ? str : "invalid packet received", NULL);
         return -1;
      }

      /* Now check for correct block number */
      if (ntohs(pkt->msg.num) > bnum) {
         Tcl_AppendResult(interp, "some packets lost!!!", NULL);
         return -1;
      } else if (ntohs(pkt->msg.num) < bnum) {
         bnum--;
         flag = 0;
      } else
         flag = 1;

      for (i = 0; i < attemps; i++) {
         if (flag) {
            /* Write data on local file */
            if ((err = (tftp_write_data(pkt, pkt_l-TFTP_HEADER, st) < 0))) {
               tftp_make_error_pkt(NOT_DEFINED, pkt, &n);
               Tcl_AppendResult(interp, "cannot save data on local file", NULL);
            }
            else {
               /* Build an ack packet and send it */
               spkt.op = htons(TFTP_ACK);
               spkt.msg.num = htons(bnum);
               n = TFTP_HEADER;
            }
      
            if (write(sock, &spkt, TFTP_S_L) < 0) {
               Tcl_AppendResult(interp, "cannot send ack", NULL);
               return -1;
            }
         
            /* Check if we have finished */
            if (err)
               return -1;
            if (pkt_l < TFTP_S_L)
               return 0;
         }
   
         FD_ZERO(&rs); FD_SET(sock, &rs);
         to.tv_sec = timeout; to.tv_usec = 0;
         if (select(sock+1, &rs, NULL, NULL, &to) < 0) {
            Tcl_AppendResult(interp, "transfer error", NULL);
            return -1;
         }
   
         if (!FD_ISSET(sock, &rs))   /* the time is over... */
            continue;

         if ((pkt_l = read(sock, pkt, TFTP_S_L)) < 0) {
            Tcl_AppendResult(interp, "cannot receive data", NULL);
            return -1;
         }
         break;
      }
      if (i >= attemps) {
         Tcl_AppendResult(interp, "transfer timed out", NULL);
         return -1;
      }
   }

   return 0;
}

static int tftp_send_file(Tcl_Interp *interp, int sock, struct tftp_s *pkt, int pkt_l, FILE *st)
{
   unsigned short bnum = 0;
   char *err;

   struct tftp_s spkt;
   int spkt_l = TFTP_S_L;

   struct timeval to;
   fd_set rs;
   int i;

   /* The ack packet should be already received into connect/accept */

   for (; 1; bnum++) {
      /* Check if we received an ack message */
      if (ntohs(pkt->op) != TFTP_ACK) {
         if ((err = tftp_check_error(pkt)) != NULL)
            Tcl_AppendResult(interp, err, NULL);
         else 
            Tcl_AppendResult(interp, "unknow packet from server", NULL);
         return -1;
      }

      /* Now check for correct block number */
      if (ntohs(pkt->msg.num) > bnum) {
         Tcl_AppendResult(interp, "some packets lost!!!", NULL);
         return -1;
      }
      if (ntohs(pkt->msg.num) == bnum) {
         /* Check if we have finished */
         if (spkt_l < TFTP_S_L)
            break;

         /* Read data from local file and build new packet */
         spkt.op = htons(TFTP_DATA);
         spkt.msg.num = htons(bnum+1);
         if ((spkt_l = tftp_read_data(&spkt, 512, st)) < 0) {
            Tcl_AppendResult(interp, "cannot write data on local file", NULL);
            return -1;
         }
      }
      else   /* ntohs(pkt->msg.num) < bnum */
         bnum--;
   
      for (i = 0; i < attemps; i++) {
         /* Send the packet */
         if (write(sock, &spkt, spkt_l+TFTP_HEADER) < 0) {
            Tcl_AppendResult(interp, "cannot send data", NULL);
            return -1;
         }
   
         FD_ZERO(&rs); FD_SET(sock, &rs);
         to.tv_sec = timeout; to.tv_usec = 0;
         if (select(sock+1, &rs, NULL, NULL, &to) < 0) {
            Tcl_AppendResult(interp, "transfer error", NULL);
            return -1;
         }

         if (!FD_ISSET(sock, &rs))   /* the time is over... */
            continue;

         if ((pkt_l = read(sock, pkt, TFTP_S_L)) < 0) {
            Tcl_AppendResult(interp, "cannot receive data", NULL);
            return -1;
         }
         break;
      }
      if (i >= attemps) {
         Tcl_AppendResult(interp, "transfer timed out", NULL);
         return -1;
      }
   }

   return 0;
}

/* **************************************************************************
 * tftp server
 *
 * Usage:
 *   ... server 
 * ************************************************************************** */
static int tftp_server(Tcl_Interp *interp, int p, int objc, Tcl_Obj *CONST objv[])
{
   unsigned short mode;
   FILE *st;
   int sock;

   struct tftp_s pkt;
   int n;

   /* Wait for a request */
   if ((sock = tftp_accept(&mode, &st, &pkt, &n)) < 0)
   if (sock < 0) {
      Tcl_AppendResult(interp, "cannot accept connections", NULL);
      return -1;
   }

   /* Transfer the file */
   if (st) {   /* file to transfer is ok */
      if (mode == TFTP_WRQ) {
         if (tftp_recv_file(interp, sock, &pkt, n, st) < 0)
            return -1;
      }
      else {   /* TFTP_RRQ */
         if (tftp_send_file(interp, sock, &pkt, n, st) < 0)
            return -1;
      }

      close(sock);
      fclose(st);
   }
   
   return 0;
}

/* **************************************************************************
 * tftp client
 *
 * Usage:
 *   ... client cmd file server
 *
 *   cmd        - either "get" or "put"
 *   file       - the file name to get/put from/to remote server
 *   server     - the remote server address
 * ************************************************************************** */
static int tftp_client(Tcl_Interp *interp, int p, int objc, Tcl_Obj *CONST objv[])
{
   int i;
   CONST84 char *cmds[] = {
      "get", "put", NULL
   };
   enum {
      TFTP_GET = 0, TFTP_PUT,
   };

   FILE *st;
   struct hostent *hostent_s;
   int sock;

   struct tftp_s pkt;
   int n;

   /* Check command line */
   if (p+3 > objc) {
      Tcl_WrongNumArgs(interp, 1, objv, "client cmd file server");
      return -1;
   }

   /* Decode the command */
   if (Tcl_GetIndexFromObj(interp, objv[p], cmds, "command", 0, &i) != TCL_OK)
      return -1;

   /* Try opening local file in current directory */
   fname = (fname == NULL ? Tcl_GetString(objv[p+1]) : fname);
   if ((st = fopen(fname, i == TFTP_GET ? "w" : "r")) == NULL) {
      Tcl_AppendResult(interp, strerror(errno), NULL);
      return -1;
   }

   if ((hostent_s = gethostbyname(Tcl_GetString(objv[p+2]))) == NULL) {
      Tcl_AppendResult(interp, "cannot resolve server address", NULL);
      fclose(st);
      return -1;
   }

   /* Ok, do the connection request */
   sock = tftp_connect(&hostent_s->h_addr,
                       i == TFTP_GET ? TFTP_RRQ : TFTP_WRQ,
                       Tcl_GetString(objv[p+1]), &pkt, &n);
   if (sock < 0) {
      Tcl_AppendResult(interp, "cannot connect", NULL);
      return -1;
   }

   /* Transfer the file */
   if (i == TFTP_GET) {
      if (tftp_recv_file(interp, sock, &pkt, n, st) < 0)
         return -1;
   }
   else {   /* TFTP_PUT */
      if (tftp_send_file(interp, sock, &pkt, n, st) < 0)
         return -1;
   }

   close(sock);
   fclose(st);

   return 0;
}

/* **************************************************************************
 * Usage:
 *   tftp ?-port num? ?-timeout secs? server
 *   tftp ?-ascii? ?-output file? ?-port num? ?-timeout secs? \
 *                                    client cmd file server
 *
 *   -ascii	- send/receive remote file in netascii mode (default is
 *                binary mode). Only for "client".
 *   -output    - save the received into file "file". Only for "client".
 *   -port      - use UDP port "num" instead of default 69
 *   -timeout	- set the timeout, in seconds,  before abort current
 *		  operation (default is 1 sec).
 *   cmd	- either "get" or "put"
 *   file	- the file name to get/put from/to remote server
 *   server	- the remote server address
 * ************************************************************************** */
int ettcl_cmd_tftp(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   int p, i;
   CONST84 char *opts[] = {
      "-ascii", "-output", "-port", "-timeout", "-attemps", NULL
   };
   enum {
      TFTP_ASCII = 0, TFTP_OUTPUT, TFTP_PORT, TFTP_TIMEOUT, TFTP_ATTEMPS
   };
   CONST84 char *cmds[] = {
      "server", "client", NULL
   };
   enum {
      TFTP_SERVER = 0, TFTP_CLIENT
   };

   if (objc < 2) {
      Tcl_WrongNumArgs(interp, 1 , objv,
                       "?-ascii? ?-output file? ?-port num? ?-timeout secs? "
                       "cmd args");
      return TCL_ERROR;
   }

   /* Set the default values */
   type = TFTP_OCTET;
   fname = NULL;
   port = 69;
   timeout = 1;
   attemps = 3;

   /* Decode the options */
   p = 1;   /* first non option */
   while (p < objc) {
      if (Tcl_GetIndexFromObj(interp, objv[p], opts, "option", 0, &i) != TCL_OK)
         break;

      switch (i) {
         case TFTP_ASCII : {
            type = TFTP_NETASCII;
            p += 1;
            break;
         }
         case TFTP_OUTPUT : {
            if (p+1 >= objc) {
               Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                        ": must specify out file name", NULL);
               return TCL_ERROR;
            }
            fname = Tcl_GetString(objv[p+1]);
            p += 2;
            break;
         }
         case TFTP_PORT : {
            if (p+1 >= objc) {
               Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                        ": must specify port number", NULL);
               return TCL_ERROR;
            }
            if (Tcl_GetIntFromObj(interp, objv[p+1], &port) != TCL_OK) {
               Tcl_AppendResult(interp, " while parsing port number", NULL);
               return TCL_ERROR;
            }
            if (port <= 0 || port > 65535) {
               Tcl_AppendResult(interp, " invalid port value", NULL);
               return TCL_ERROR;
            }
            p += 2;
            break;
         }
         case TFTP_TIMEOUT : {
            if (p+1 >= objc) {
               Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                        ": must specify timeout", NULL);
               return TCL_ERROR;
            }
            if (Tcl_GetIntFromObj(interp, objv[p+1], &timeout) != TCL_OK) {
               Tcl_AppendResult(interp, " while parsing timeout", NULL);
               return TCL_ERROR;
            }
            if (timeout <= 0) {
               Tcl_AppendResult(interp, " invalid timeout value", NULL);
               return TCL_ERROR;
            }
            p += 2;
            break;
         }
         case TFTP_ATTEMPS : {
            if (p+1 >= objc) {
               Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                        ": must specify a number", NULL);
               return TCL_ERROR;
            }
            if (Tcl_GetIntFromObj(interp, objv[p+1], &attemps) != TCL_OK) {
               Tcl_AppendResult(interp, " while parsing attemps", NULL);
               return TCL_ERROR;
            }
            if (timeout <= 0) {
               Tcl_AppendResult(interp, " invalid value", NULL);
               return TCL_ERROR;
            }
            p += 2;
            break;
         }
      }
   }
   if (*Tcl_GetString(objv[p]) == '-')
      return TCL_ERROR;
   Tcl_ResetResult(interp);

   /* Check command line */
   if (p >= objc) {
      Tcl_WrongNumArgs(interp, 1 , objv,
                       "?-ascii? ?-output file? ?-port num? ?-timeout secs? "
                       "cmd args");
      return TCL_ERROR;
   }

   /* Decode the command */
   if (Tcl_GetIndexFromObj(interp, objv[p], cmds, "subcommand", 0, &i) != TCL_OK)
      return TCL_ERROR;
   p++;

   /* Do the job */
   Tcl_AppendResult(interp, Tcl_GetString(objv[0]), ": ", NULL);
   switch (i) {
      case TFTP_SERVER : {
         if (tftp_server(interp, p, objc, objv) < 0)
            return TCL_ERROR;
         break;
      }
      case TFTP_CLIENT : {
         if (tftp_client(interp, p, objc, objv) < 0)
            return TCL_ERROR;
         break;
      }
   }
   Tcl_ResetResult(interp);
   
   return TCL_OK;
}
