/* 
 * sys_uuencode.c --
 *
 *      Implement the "uuencode" and "uudecode" commands.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>

#include "ettcl.h"

/* **************************************************************************
 * Encoding function
 * ************************************************************************** */
void uuencode(char *uubuf, int n, FILE *std_out)
{
   int p;
   unsigned int l;

   fputc(' '+n, std_out);

   for (p = 0; n > 0; p += 3, n -= 3) {
      if (n < 3)
         uubuf[p+2] = 0;
      if (n < 2)
         uubuf[p+1] = 0;
   
      l = uubuf[p]<<16|uubuf[p+1]<<8|uubuf[p+2];
   
      /* This uses '`' instead of ' '  */
      fputc(' '+(((l>>18)-1)&0x3f)+1, std_out);
      fputc(' '+(((l>>12)-1)&0x3f)+1, std_out);
      fputc(' '+(((l>> 6)-1)&0x3f)+1, std_out);
      fputc(' '+(((l>> 0)-1)&0x3f)+1, std_out);
   }
}

/* **************************************************************************
 * Decoding function
 * ************************************************************************** */
static void uudecode(FILE *st_out, char *uubuf)
{
   unsigned long l;
   int ptr, count;

   if (uubuf[0] == '`')
      return;

   count = uubuf[0]-' ';
   for (ptr = 1; count; ptr += 4) {
      l = (((uubuf[ptr]-' ')&0x3f)<<18)|
           (((uubuf[ptr+1]-' ')&0x3f)<<12)|
           (((uubuf[ptr+2]-' ')&0x3f)<<6)|
            ((uubuf[ptr+3]-' ')&0x3f);
      if (count) {
         putc(l>>16, st_out);
         count--;
      }
      if (count) {
         putc((l>>8)&0xff, st_out);
         count--;
      }
      if (count) {
         putc(l&0xff, st_out);
         count--;
      }
   }
}

/* **************************************************************************
 * Usage:
 *   uuencode ?file? name
 *
 *   name	- the name of encoded data
 *   file	- the file to encode (the file will be opened, encoded
 *		  and, in the end, closed
 * ************************************************************************** */
int ettcl_cmd_uuencode(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   FILE *st_in = stdin;
   FILE *st_out = stdout;   /* currently is fixed... */
   int i = 1;
   struct stat sb;
   int md = 0664;
   int n, m;
   #define UUBUFLEN	80
   char uubuf[UUBUFLEN+1];

   if (objc < 2) {
      Tcl_WrongNumArgs(interp, 1, objv, "?file? name");
      return TCL_ERROR;
   }

   if (objc > 2) {
      st_in = fopen(Tcl_GetString(objv[1]), "r");
      if (st_in == NULL || fstat(fileno(st_in), &sb) < 0) {
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                  ": ", strerror(errno), NULL);
         return TCL_ERROR;
      }
      md = sb.st_mode&(S_IRWXU|S_IRWXG|S_IRWXO);
      i = 2;
   }

   /* Do the job */
   fprintf(st_out, "begin %o %s\n", md, Tcl_GetString(objv[i]));
   while (1) {
      n = 0;
      while (n < 45) {
         if ((m = fread(uubuf, 1, 45-n, st_in)) == 0)
           break;
         n += m;
      }
      if (n == 0)
        break;

      uuencode(uubuf, n, st_out);
      fputc('\n', st_out);
   }
   fprintf(st_out, "`\nend\n");

   if (st_in != stdin)
      fclose(st_in);

   return TCL_OK;
}

/* **************************************************************************
 * Usage:
 *   uudecode ?name?
 *
 *   name	- the file name to be read (the file will be opened, decoded
 *		  and, in the end, closed
 * ************************************************************************** */
int ettcl_cmd_uudecode(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   FILE *st_in = stdin, *st_out;
   int md;
   #define UUBUFLEN	80
   char fname[UUBUFLEN+1];   /* enought to store file name */
   char uubuf[UUBUFLEN+1];

   if (objc > 1) {
      st_in = fopen(Tcl_GetString(objv[1]), "r");
      if (st_in == NULL) {
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                  ": ", strerror(errno), NULL);
         return TCL_ERROR;
      }
   }

   /* Do the job */
   st_out = NULL;
   while (fgets(uubuf, UUBUFLEN, st_in) != NULL) {
      if (strncmp(uubuf, "end", 3) == 0) {
         if (st_out) {
            fclose(st_out);
            chmod(fname, md);
         }
         return TCL_OK;
      }

      if (strncmp(uubuf, "begin", 5) == 0) {
         if (sscanf(uubuf, "begin %o %s", &md, fname) != 2) {
            Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                     ": error in input data\n", NULL);
            fclose(st_out);
            return TCL_ERROR;
         }
         if ((st_out = fopen(fname, "w")) == NULL) {
            Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                     ": ", strerror(errno), NULL);
            return TCL_ERROR;
         }
         continue;
      }
      if (st_out == NULL)
         continue;

      uudecode(st_out, uubuf);
   }

   if (st_out) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": incomplete input, \"",
                               fname, "\" is corrupted", NULL);
      fclose(st_out);
      return TCL_ERROR;
   }
   if (st_in != stdin)
      fclose(st_in);

   return TCL_OK;
}
