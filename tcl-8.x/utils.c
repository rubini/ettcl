/* 
 * Support for signal management for TCL/TK.
 *
 * utils.c - Several utility functions.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini (rubini@linux.it)
 * Copyright (c) 2002,2003         Rodolfo Giometti (giometti@linux.it)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * See the file "LICENSE" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 */

#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>

#include "ettcl.h"

/* --- Private data --------------------------------------------------------- */

/* This vector is used to convert between signals names and values */
static struct {
    char *name;
    int val;
} signal_table[] = {
   /* First the signals described in POSIX.1 (man 7 signal) */
   {"HUP",   SIGHUP},
   {"INT",   SIGINT},
   {"QUIT",  SIGQUIT},
   {"ILL",   SIGILL},
   {"ABRT",  SIGABRT},
   {"FPE",   SIGFPE},
   {"KILL",  SIGKILL},
   {"SEGV",  SIGSEGV},
   {"PIPE",  SIGSEGV},
   {"ALRM",  SIGALRM},
   {"TERM",  SIGTERM},
   {"USR1",  SIGUSR1},
   {"USR2",  SIGUSR2},
   {"CHLD",  SIGCHLD},
   {"CONT",  SIGCONT},
   {"STOP",  SIGSTOP},
   {"TSTP",  SIGTSTP},
   {"TTIN",  SIGTTIN},
   {"TTOU",  SIGTTOU},

   /* Next the signals not in POSIX.1 but described in SUSv2 (man 7 signal) */
   {"BUS",   SIGBUS},
   {"POLL",  SIGPOLL},
   {"PROF",  SIGPROF},
   {"SYS",   SIGSYS},
   {"TRAP",  SIGTRAP},
   {"URG",   SIGURG},
   {"VTALRM",SIGVTALRM},
   {"XCPU",  SIGXCPU},
   {"XFSZ",  SIGXFSZ},

   /* Next various other signals (man 7 signal) */
#ifdef SIGIOT
   {"IOT",   SIGIOT},
#endif
#ifdef SIGEMT
   {"EMT",   SIGEMT},
#endif
#ifdef SIGSTKFLT
   {"STKFLT",SIGSTKFLT},
#endif
#ifdef SIGIO
   {"IO",    SIGIO},
#endif
#ifdef SIGCLD
   {"CLD",   SIGCLD},
#endif
#ifdef SIGPWR
   {"PWR",   SIGPWR},
#endif
#ifdef SIGINFO
   {"INFO",  SIGINFO},
#endif
#ifdef SIGLOST
   {"LOST",  SIGLOST},
#endif
#ifdef SIGWINCH
   {"WINCH", SIGWINCH},
#endif
#ifdef SIGUNUSED
   {"UNUSED",SIGUNUSED},
#endif
   
   /* At last the "end list" entry */
   {NULL, 0},
};

/* --- Public functions ----------------------------------------------------- */

/* Convert from signal name (string) to the relative signal number */
int signal_name2num(Tcl_Interp *interp, char *sig_name)
{
   int i;
 
   for (i = 0; signal_table[i].val; i++)
      if (strcmp(sig_name, signal_table[i].name) == 0)
         return signal_table[i].val;

   /* otherwise, try a number */
   if (Tcl_GetInt(interp, sig_name, &i) != TCL_OK)
      return -1;

   if (i < 0)
      i *= -1;
   if (i >= NSIG)
      i = -1;

   return i;
}

/* Convert from signal number to the relative signal name (string) */
char *signal_num2name(const int val)
{
   int i;

   if (val < 0 || val >= NSIG)
      return NULL;

   for (i = 0; signal_table[i].val; i++)
      if (signal_table[i].val == val)
         return signal_table[i].name;

   return NULL;
}

/* Convert from TCL channel to a proper file descriptor */
int get_fd_from_channel(Tcl_Channel ch, int dir)
{
   int fd = -1;

   if (dir&TCL_READABLE)
      if (Tcl_GetChannelHandle(ch, TCL_READABLE, (ClientData *) &fd) != TCL_OK)
         fd = -1;

   if (dir&TCL_WRITABLE)
      if (Tcl_GetChannelHandle(ch, TCL_WRITABLE, (ClientData *) &fd) != TCL_OK)
         fd = -1;

   return fd;
}

/* Convert from a TCL file name to a file descriptor number */
int get_fd_from_name(char *name)
{
   int fd;

   /* Either "file1" and "sock1" or "foo1" is ok! */
   if (sscanf(name, "%*s%d", &fd) == 1)
      return fd;    

   /* Try standard names */
   if (strcmp(name, "stdin") == 0)
      return STDIN_FILENO;
   if (strcmp(name, "stdout") == 0)
      return STDOUT_FILENO;
   if (strcmp(name, "stderr") == 0)
      return STDERR_FILENO;

   return -1;
}
