#ifndef _ETTCL_H_
#define _ETTCL_H_

#include <tcl.h>

#include "sysdep.h"

/* --- Ettcl common data struct --------------------------------------------- */
struct signal_data {
    int init;
    int pid;
    Tcl_AsyncHandler token;
    Tcl_Obj *command;
    Tcl_Interp *interp;
};

/* --- Exported data -------------------------------------------------------- */
extern struct signal_data signal_data[];

/* --- Several utility & exported functions --------------------------------- */
extern int signal_name2num(Tcl_Interp *, char *);
extern char *signal_num2name(const int);
extern int async_proc(ClientData, Tcl_Interp *, int);
extern int get_fd_from_channel(Tcl_Channel, int);
extern int get_fd_from_name(char *);

/* --- New Ettcl commands --------------------------------------------------- */
extern int ettcl_sys_chmod(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_dup(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_exec(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_fork(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_kill(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_mknod(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_nice(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_opentty(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_reboot(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_signal(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_sync(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_ttypipe(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_umask(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_sys_wait(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);

extern int ettcl_cmd_iofuncs(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_cmd_scheduler(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_cmd_tftp(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_cmd_uudecode(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_cmd_uuencode(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);

extern int ettcl_net_ifconfig(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);
extern int ettcl_net_route(ClientData, Tcl_Interp *, int, Tcl_Obj *CONST []);

/* --- Ettcl initilization function ----------------------------------------- */
extern int Ettcl_Init(Tcl_Interp *);

#endif   /* _ETTCL_H_ */
