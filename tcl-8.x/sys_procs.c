/* 
 * sys_procs.c --
 *
 *      Implement the "sys_fork", "sys_wait" and "sys_exec" commands.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 1998,1999         Prosa Srl <prosa@prosa.it>
 * Copyright (c) 1999              Marco Pantaleoni <panta@prosa.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "ettcl.h"

/* **************************************************************************
 * Usage:
 *   sys_fork
 * ************************************************************************** */
int ettcl_sys_fork(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   pid_t pid;

   if ((pid = fork()) < 0) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": ", strerror(errno), NULL);
      return TCL_ERROR;
   }

   Tcl_SetObjResult(interp, Tcl_NewIntObj(pid));

   return TCL_OK;
}

/* **************************************************************************
 * Usage:
 *   sys_wait ?pid?
 *
 *   pid	- the PID of the process to wait (if "nohang" is specified
 *		  the syscall waitpid(..., WNOHANG) is called - see docs)
 * ************************************************************************** */
int ettcl_sys_wait(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   pid_t pid;
   int status;
   Tcl_Obj *list;

   if (objc == 1) {
      pid = wait(&status);
   }
   else if (objc == 2 && !strcmp(Tcl_GetString(objv[1]), "nohang")) {
      pid = waitpid(-1, &status, WNOHANG);
      if (pid == 0) {
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                  ": ", strerror(EAGAIN), NULL);
         return TCL_ERROR;
      }
   }
   else {
      if (Tcl_GetIntFromObj(interp, objv[1], &pid) != TCL_OK) {
         Tcl_AppendResult(interp, " while parsing pid", NULL);
         return TCL_ERROR;
      }

      pid = waitpid(pid, &status, 0);
   }

   if (pid < 0) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": ", strerror(errno), NULL);
      return TCL_ERROR;
   }

   /* build the result as a list */
   list = Tcl_NewListObj(0, NULL);
   Tcl_ListObjAppendElement(interp, list, Tcl_NewIntObj(pid));
   Tcl_ListObjAppendElement(interp, list, Tcl_NewIntObj(WEXITSTATUS(status)));
   Tcl_SetObjResult(interp, list);

   return TCL_OK;
}

/* **************************************************************************
 * Usage:
 *   sys_exec command ?args?
 *
 *   command	- the command to be executed
 *   args	- the command's arguments
 * ************************************************************************** */
int ettcl_sys_exec(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   #define NUM_ARGS 20
   char *(argStorage[NUM_ARGS]);
   char **argv = argStorage;
   int i;

   if (objc < 2) {
      Tcl_WrongNumArgs(interp, 2, objv, "command ?args?");
      return TCL_ERROR;
   }

   /* This code generates an argv array for the string arguments. It starts
      out with stack-allocated space but uses dynamically-allocated storage
      if needed. (Code from TCL-8.0 main source, file "tclBasic.c" */

   /* Create the string argument array "argv". Make sure argv is large
      enough to hold the objc arguments plus 1 extra for the zero end-of-argv
      word.

      THIS FAILS IF ANY ARGUMENT OBJECT CONTAINS AN EMBEDDED NULL */

   if (objc+1 > NUM_ARGS)
      argv = (char **) ckalloc((unsigned)(objc + 1) * sizeof(char *));

   for (i = 0;  i < objc;  i++) 
      argv[i] = Tcl_GetString(objv[i]);
   argv[objc] = 0;

   /* Invoke the command's string-based */
   execvp(argv[1], argv+1);    /* If it returns, we are on error */

   /* Free the argv array if malloc'ed storage was used */
   if (argv != argStorage)
      ckfree((char *) argv);
   #undef NUM_ARGS

   Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                            ": ", strerror(errno), NULL);

   return TCL_ERROR;
}
