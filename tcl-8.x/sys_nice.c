/* 
 * sys_nice.c --
 *
 *      Implement the "sys_nice" command.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 1998,1999         Prosa Srl <prosa@prosa.it>
 * Copyright (c) 1999              Marco Pantaleoni <panta@prosa.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/types.h>

#include "ettcl.h"

/* **************************************************************************
 * Usage:
 *   sys_nice level
 *
 *   level	- the new nice value for process
 * ************************************************************************** */
int ettcl_sys_nice(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   int inc;

   if (objc < 2) {
      Tcl_WrongNumArgs(interp, 1 , objv, "level");
      return TCL_ERROR;
   }

   /* Decode the prio name */
   if (Tcl_GetIntFromObj(interp, objv[1], &inc) != TCL_OK) {
      Tcl_AppendResult(interp, " while parsing the nice level", NULL);
      return TCL_ERROR;
   }

   if (nice(-inc) < 0) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]), strerror(errno), NULL);
      return TCL_ERROR;
   }

   return TCL_OK;
}
