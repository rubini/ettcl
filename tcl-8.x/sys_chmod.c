/* 
 * sys_chmod.c --
 *
 *      Implement the "sys_chmod" command.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#include "ettcl.h"

/* **************************************************************************
 * Usage:
 *   sys_chmod mode names		(new way)
 *   sys_chmod name mode		(old way - deprecated)
 *
 *   mode	- new file mode (in numeric form)
 *   name 	- the file name (only one) to be processed
 *   names 	- the files name to be processed
 * ************************************************************************** */
int ettcl_sys_chmod(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   int mode;
   int i;

   if (objc < 3) {
      Tcl_WrongNumArgs(interp, 1, objv, "mode names");
      return TCL_ERROR;
   }

   /* Decode the new file mode */
   if (Tcl_GetIntFromObj(interp, objv[1], &mode) == TCL_OK) { /* new way */
      for (i = 2; i < objc; i++) {
         if (chmod(Tcl_GetString(objv[i]), (mode_t) mode) != 0) {
            Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                     ": ", Tcl_GetString(objv[i]),
                                     ": ", strerror(errno), NULL); 
            return TCL_ERROR;
         }
      }
      return TCL_OK;
   }

   /* Try the old way */
   Tcl_ResetResult(interp);
   if (Tcl_GetIntFromObj(interp, objv[2], &mode) == TCL_OK) {
      if (chmod(Tcl_GetString(objv[1]), (mode_t) mode) != 0) {
            Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                     ": ", strerror(errno), NULL);
            return TCL_ERROR;
      }
      return TCL_OK;
   }

   /* We need recreate the error with objv[1] */
   Tcl_GetIntFromObj(interp, objv[1], &mode);
   Tcl_AppendResult(interp, "\nwhile parsing a file mode", NULL);

   return TCL_ERROR;                                                     
}
