/* 
 * sys_dup.c --
 *
 *      Implement the "sys_dup" command.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 1998,1999         Prosa Srl <prosa@prosa.it>
 * Copyright (c) 1999              Marco Pantaleoni <panta@prosa.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "ettcl.h"

/* **************************************************************************
 * Usage:
 *   sys_dup old new
 *
 *   old	- the file to copy
 *   new 	- the file to became the copy of "old"
 * ************************************************************************** */
int ettcl_sys_dup(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   Tcl_Channel och, nch;
   int ofd = -1, nfd = -1, omd, nmd;

   if (objc < 3) {
      Tcl_WrongNumArgs(interp, 1, objv, "old new");
      return TCL_ERROR;
   }

   /* "Source" files must exist */
   och = Tcl_GetChannel(interp, Tcl_GetString(objv[1]), &omd);
   if (!och)
      return TCL_ERROR;
   nch = Tcl_GetChannel(interp, Tcl_GetString(objv[2]), &nmd);
   if (!nch)
      Tcl_ResetResult(interp);

   /* Ok now we need file descriptors */
   ofd = get_fd_from_channel(och, omd);
   if (ofd < 0) {
      Tcl_AppendResult(interp,  Tcl_GetString(objv[0]),
                                  ": cannot get stream(s) data", NULL);
      return TCL_ERROR;
   }
   if (nch) {
      nfd = get_fd_from_channel(nch, nmd);
      Tcl_UnregisterChannel(interp, nch);   /* we don't need it anymore... */
   }
   if (nfd < 0)
      nfd = get_fd_from_name(Tcl_GetString(objv[2]));

   /* Now duplicate the source fd (assume it is unique) */
   if (dup2(ofd, nfd) < 0) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": ", strerror(errno), NULL);
      return TCL_ERROR;
   }

   /* Now recreate the new channel into current interp with new access mode */
   nch = Tcl_MakeFileChannel((ClientData *) nfd, omd);
   if (nch == NULL) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": cannot create new stream", NULL);
      close(nfd);
      return TCL_ERROR;
   }
   Tcl_RegisterChannel(interp, nch);

   return TCL_OK;
}
