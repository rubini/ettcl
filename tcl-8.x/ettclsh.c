/* 
 * main.c --
 *
 *	Provides a default version of the main program and Tcl_AppInit
 *	procedure for Ettcl.
 *
 * Copyright (c) 2003 Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ettcl.h"

/* **************************************************************************
 * The specific Tcl initialization procedure
 * ************************************************************************** */

int Tcl_AppInit(Tcl_Interp *interp)
{
   if (Tcl_Init(interp) == TCL_ERROR)
      return TCL_ERROR;

   /* Call the EtTcl init procedures */
   if (Ettcl_Init(interp) == TCL_ERROR)
      return TCL_ERROR;

   /* Specify a user-specific startup file to invoke if the application
      is run interactively. I do not use the name "~/.ettclshrc" for
      backward compatibility */
   Tcl_SetVar(interp, "tcl_rcFileName", "~/.tclshrc", TCL_GLOBAL_ONLY);

   return TCL_OK;
}

/* **************************************************************************
 * The main procedure
 * ************************************************************************** */

int main(int argc, char *argv[])
{
    Tcl_Main(argc, argv, Tcl_AppInit);

    return 0;   /* needed only to prevent compiler warning */
}
