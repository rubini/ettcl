/* 
 * sys_umask.c --
 *
 *      Implement the "sys_umask" command.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#include "ettcl.h"

/* **************************************************************************
 * Usage:
 *   sys_umask mask
 *
 *   mask	- new umask (use 0xxx for octal)
 * ************************************************************************** */
int ettcl_sys_umask(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   int mask;

   if (objc < 2) {
      Tcl_WrongNumArgs(interp, 1, objv, "mask");
      return TCL_ERROR;
   }

   /* Get the new umask */
   if (Tcl_GetIntFromObj(interp, objv[1], &mask) != TCL_OK) {
      Tcl_AppendResult(interp, "\nwhile parsing file creation mask", NULL);
      return TCL_ERROR;
   }

   Tcl_SetObjResult(interp, Tcl_NewIntObj(umask((mode_t) mask)));

   return TCL_OK;                                                        
}
