/* 
 * sys_scheduler.c --
 *
 *      Implement the "scheduler" command.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sched.h>
#include <sys/mman.h>

#include "ettcl.h"

static int scheduler_mlockall(int status) {
   if (status > 0) /* ON */
       return mlockall(MCL_CURRENT|MCL_FUTURE);
   if (status < 0) /* OFF */
       return munlockall();

   return 0;
}

/* **************************************************************************
 * Set scheduling modes
 * ************************************************************************** */
static int set_scheduling_modes(int mstatus, int sched, struct sched_param *p)
{
   if (scheduler_mlockall(mstatus) < 0)
      return -1;
   if (sched_setscheduler(0, sched, p) < 0)
      return -1;

   return 0;
}

/* **************************************************************************
 * Usage:
 *   scheduler ?-priority ?prio?? ?-mlock status? ?-yield? ?type ?cmd??
 *
 *   prio	- new priority to be set
 *   status 	- new memory locking status
 *   type 	- scheduling type
 *   cmd 	- command to execute
 * ************************************************************************** */
int ettcl_cmd_scheduler(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   int n, i;
   CONST84 char *cmds[] = {
      "-priority", "-mlock", "-yield", NULL
   };
   struct sched_param p, op;
   int sched, osched;
   int mstatus = 0;
   int ret = TCL_OK;

   /* First of all get current scheduling settings */
   if (sched_getparam(0, &p) < 0) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": cannot get current priority", NULL);
      return TCL_ERROR;
   }
   op = p;
   if ((sched = sched_getscheduler(0)) < 0) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": cannot get current scheduler", NULL);
      return TCL_ERROR;
   }
   osched = sched;

   n = 1;
   while (n < objc) {
      /* More options? */
      if (Tcl_GetString(objv[n])[0] != '-')
         break;

      /* Ok, get the option... */
      if (Tcl_GetIndexFromObj(interp, objv[n], cmds, "option", 0, &i) != TCL_OK)
         return TCL_ERROR;

      /* The big switch */
      switch (i) {
         case 0 : {   /* -priority */
            if (n+1 >= objc) {
               Tcl_SetObjResult(interp, Tcl_NewIntObj(p.sched_priority));
               return TCL_OK;
            }

            if (Tcl_GetIntFromObj(interp, objv[n+1], &p.sched_priority) != TCL_OK) {
               Tcl_AppendResult(interp, " while parsing priority", NULL);
               return TCL_ERROR;
            }

            n += 2;
            break;
         }

         case 1 : {   /* -mlock */
            if (n+1 >= objc) {
               Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                        ": must supply mlock status", NULL);
               return TCL_ERROR;
            }

            if (Tcl_GetBooleanFromObj(interp, objv[n+1], &mstatus) != TCL_OK) {
               Tcl_AppendResult(interp, " while parsing mlock status", NULL);
               return TCL_ERROR;
            }
            mstatus = mstatus ? 1 : -1;

            n += 2;
            break;
         }

         case 2 : {   /* -yield */
            /* Special case (actually not an option...): must release the CPU */
            if (sched_yield() < 0) {
               Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                        ": cannot yield CPU", NULL);
               return TCL_ERROR;
            }

            return TCL_OK;
         }
      }
   }

   /* Now check if we have to return current scheduler */
   if (objc-n == 0) {
      if (sched == SCHED_OTHER)
         Tcl_SetResult(interp, "OTHER", TCL_STATIC); 
      else if (sched == SCHED_FIFO)
         Tcl_SetResult(interp, "FIFO", TCL_STATIC); 
      else if (sched == SCHED_RR)
         Tcl_SetResult(interp, "RR", TCL_STATIC); 

      return TCL_OK;
   }

   /* Now check if "type" has been supplied */
   if (objc-n > 0) {
      if (strcmp(Tcl_GetString(objv[n]), "OTHER") == 0)
         sched = SCHED_OTHER;
      else if (strcmp(Tcl_GetString(objv[n]), "FIFO") == 0)
         sched = SCHED_FIFO;
      else if (strcmp(Tcl_GetString(objv[n]), "RR") == 0)
         sched = SCHED_RR;
      else {
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                  ": invalid scheduler", NULL);
         return TCL_ERROR;
      }
   }

   /* Ok, do the settings */
   if (set_scheduling_modes(mstatus, sched, &p) < 0) {
       Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                ": cannot set scheduling modes", NULL);
      return TCL_ERROR;
   }

   /* And in the end check if "cmd" has been supplied */
   if (objc-n > 1) {
      ret = Tcl_EvalObjEx(interp, objv[n+1], 0);

      /* Restore previous settings */
      if (set_scheduling_modes(-mstatus, osched, &op) < 0)
         return TCL_ERROR;
   }

   return ret;
}
