/*
 * cmd_iofuncs.c -- 
 *
 *      Implement the "inp", "outp", "inw", "outw",
 *      "readb", "writeb", "readw", "writew", "readl", "writel" commands.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 1998,1999         Prosa Srl <prosa@prosa.it>
 * Copyright (c) 1999              Marco Pantaleoni <panta@prosa.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#ifdef __i386__
#  include <sys/io.h>
#endif
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "ettcl.h"

/* **************************************************************************
 * Usage:
 *   inp port
 *   outp port data
 *   inw port
 *   outw port data
 *   readb port
 *   writeb port data
 *   readw port
 *   writew port data
 *   readl port
 *   writel port data
 *
 *   port       - the port to read/write from/to
 *   data       - the datum to write
 * ************************************************************************** */
int ettcl_cmd_iofuncs(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   char *cmd = Tcl_GetString(objv[0]); 
   int fd;
   int port;
   unsigned char byte;
   unsigned short word;
   unsigned int dword, data;

   switch (cmd[0]) {
      /*
       * "inp" and "inw" receive a port number on cmdline (hex or dec)
       * and returns the current input value.
       */
      case 'i' : {   /* in[pw] */
         if (objc != 2) {
            Tcl_WrongNumArgs(interp, 1 , objv, "port");
            return TCL_ERROR;
         }

         if (Tcl_GetIntFromObj(interp, objv[1], &port) != TCL_OK) {
            Tcl_AppendResult(interp, " while parsing port number", NULL);
            return TCL_ERROR;
         }

#ifdef __i386__   /* use direct instruction, but require access first */
         if (ioperm(port, 2, 1)) {   /* ask for two ports, in case inw is used */
            Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                     ": ioperm: ", strerror(errno), NULL);
            return TCL_ERROR;
         }

	 if (cmd[2] == 'p')   /* simple 8-bit port */
            data = inb(port);
         else
            data = inw(port);
#else   /* not 386, use /dev/port */
	 fd = open("/dev/port", O_RDONLY);
	 if (fd < 0) {
            Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                     ": /dev/port: ", strerror(errno), NULL);
            return TCL_ERROR;
         }
	 lseek(fd, port, SEEK_SET);

	 if (cmd[2] == 'p') {   /* simple 8-bit port */
            read(fd, &byte, 1);
            data = byte;
         } else {
            read(fd, &word, 2);
            data = word;
         }
	 close(fd);
#endif

         Tcl_SetObjResult(interp, Tcl_NewIntObj(data));
         return TCL_OK;
      }

      /*
       * "outp" and "outw" receive a port number and a byte/word value
       * and calls an outb or outw operation.
       */
      case 'o' : {   /* out[pw] */
         if (objc != 3) {
            Tcl_WrongNumArgs(interp, 1 , objv, "port data");
            return TCL_ERROR;
         }

         if (Tcl_GetIntFromObj(interp, objv[1], &port) != TCL_OK) {
            Tcl_AppendResult(interp, " while parsing port number", NULL);
            return TCL_ERROR;
         }
         if (Tcl_GetIntFromObj(interp, objv[2], &data) != TCL_OK) {
            Tcl_AppendResult(interp, " while parsing data value", NULL);
            return TCL_ERROR;
         }

#ifdef __i386__   /* use direct instruction, but require access first */
         if (ioperm(port, 2, 1)) {   /* ask for twwo bytes, in case outw is used */
            Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                             ": ioperm: ", strerror(errno), NULL);
            return TCL_ERROR;
         }
 
         if (cmd[3] == 'b') 
            outb(data, port);
         else
            outw(data, port);
#else   /* not __i386__ */
	 fd = open("/dev/port", O_RDWR);
	 if (fd < 0) {
            Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                     ": /dev/port: ", strerror(errno), NULL);
            return TCL_ERROR;
         }
	 lseek(fd, port, SEEK_SET);

	 if (cmd[2] == 'p') {   /* simple 8-bit port */
	    byte = data;
            write(fd, &byte, 1);
         } else {
            word = data;
            write(fd, &word, 2);
         }
	 close(fd);
#endif

         return TCL_OK;
      }

      /*
       * "readb", "readw", "readl" receive a memory address on
       * cmdline (hex or dec) and return the current content.
       */

      case 'r' : {   /* read[bwl] */
         if (objc != 2) {
            Tcl_WrongNumArgs(interp, 1 , objv, "port");
            return TCL_ERROR;
         }

         if (Tcl_GetIntFromObj(interp, objv[1], &port) != TCL_OK) {
            Tcl_AppendResult(interp, " while parsing a memory address", NULL);
            return TCL_ERROR;
         }

	 fd = open("/dev/mem", O_RDONLY);
	 if (fd < 0) {
            Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                     ": /dev/mem: ", strerror(errno), NULL);
            return TCL_ERROR;
         }
	 lseek(fd, port, SEEK_SET);

	 if (cmd[4] == 'b') {
            read(fd, &byte, 1);
            data = byte;
         } else if (cmd[4] == 'w') {
            read(fd, &word, 2);
            data = word;
         } else {   /* long */
            read(fd, &dword, 4);
            data = dword;
	 }
	 close(fd);

         Tcl_SetObjResult(interp, Tcl_NewIntObj(data));
         return TCL_OK;
      }

      /*
       * "writeb", "writew" and "writel" receive an address and a value,
       * and write that value to memory.
       */
      case 'w' : {   /* write[bwl] */
         if (objc != 3) {
            Tcl_WrongNumArgs(interp, 1 , objv, "port data");
            return TCL_ERROR;
         }

         if (Tcl_GetIntFromObj(interp, objv[1], &port) != TCL_OK) {
            Tcl_AppendResult(interp, " while parsing port number", NULL);
            return TCL_ERROR;
         }
         if (Tcl_GetIntFromObj(interp, objv[2], &data) != TCL_OK) {
            Tcl_AppendResult(interp, " while parsing data value", NULL);
            return TCL_ERROR;
         }

	 fd = open("/dev/mem", O_RDWR);
	 if (fd < 0) {
            Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                     ": /dev/mem: ", strerror(errno), NULL);
            return TCL_ERROR;
         }
	 lseek(fd, port, SEEK_SET);

	 if (cmd[4] == 'b') {
	    byte = data;
            write(fd, &byte, 1);
         } else if (cmd[4] == 'w') {
	    word = data;
            write(fd, &word, 2);
         } else {   /* long */
	    dword = data;
            write(fd, &dword, 4);
	 }
	 close(fd);

         return TCL_OK;
      }

      default : {
         Tcl_SetObjResult(interp, Tcl_NewStringObj("unknown command", -1));
         return TCL_ERROR;
      }
   }

   return TCL_OK;
}
