# *****************************************************************************
# User define section
#
# Here you should setup you preferences.
# *****************************************************************************

# Define here which TCL version you wish to use (must be 8.x). Default is 8.3
# since this is the version used to develop this package. :)
TCL_VERSION = 8.3

# Define here if you want a static linked interpreter (this option disable
# making of a suitable package for loading into a TCL interpreter and
# force ONLY_PACKAGE to "no")
# STATIC = yes

# Define here if you want only a loadable package instead of both executable
# and package
# ONLY_PACKAGE = yes

# Comment/uncomment here if you want force a special architecture for your
# machine
# ARCH = i386

# Define here if you have a cross-compiler and/or extra compiling flags
CROSS_COMPILE =
CFLAGS_EXTRA =

# Comment/uncomment here to disable/enable debugging mode
# DEBUG = yes

# Define here where the TCL8.x include files (tcl.h & Co.) are placed
# Note: these settings are tested under Debian (Sarge)
TCL_INCLUDE_DIR = /usr/include/tcl$(TCL_VERSION)
TCL_LIBRARY_DIR = /usr/lib

# *****************************************************************************
# Please do __not__ modify under this line unless you know what you are doing
# *****************************************************************************

ETTCL_VERSION = 14.0
ifndef ARCH
   ARCH := $(shell uname -m | sed -e s/i.86/i386/ -e s/sun4u/sparc64/ \
                                  -e s/arm.*/arm/ -e s/sa110/arm/)
endif

CC = $(CROSS_COMPILE)gcc
AR = $(CROSS_COMPILE)ar
CFLAGS = -Wall -I$(TCL_INCLUDE_DIR) -D__$(ARCH)__ $(CFLAGS_EXTRA)

LDFLAGS = -L$(TCL_LIBRARY_DIR)
LDLIBS = -ltcl$(TCL_VERSION) -L./ -lettcl.$(ETTCL_VERSION)

LIB_ETTCL = libettcl.$(ETTCL_VERSION)
ifeq ($(STATIC),yes)
   LIB_TYPE = a
   LDFLAGS += -static
   LDLIBS += -lc -lm -ldl
   ONLY_PACKAGE = no
else
   LIB_TYPE = so
   CFLAGS += -fPIC
   LDFLAGS += -rdynamic
endif

TARGETS = $(LIB_ETTCL).$(LIB_TYPE)
ifneq ($(ONLY_PACKAGE),yes)
   TARGETS += ettclsh
endif

ifeq ($(DEBUG),yes)
   CFLAGS += -O -ggdb -DETTCL_DEBUG # "-O" is needed to expand inlines
else
   CFLAGS += -O2
endif

SRCS = init.c
SRCS += utils.c
SRCS += sys_chmod.c sys_dup.c sys_mknod.c sys_nice.c sys_opentty.c \
        sys_procs.c sys_reboot.c sys_signals.c sys_sync.c \
        sys_ttypipe.c sys_umask.c
SRCS += cmd_iofuncs.c \
        cmd_scheduler.c cmd_tftp.c cmd_uufuncs.c
SRCS += net_ifconfig.c net_route.c

# *****************************************************************************
# Actions section
# *****************************************************************************

.PHONY : all
.PHONY : depend dep

.SECONDARY : $(SRCS:.c=.o)

all : .depend $(TARGETS)

.depend depend dep :
	$(CC) $(CFLAGS) -M $(SRCS) > .depend

$(LIB_ETTCL).so : $(SRCS:.c=.o)
	$(CC) $(CFLAGS) -shared -o $@ $^ -ldl

$(LIB_ETTCL).a : $(SRCS:.c=.o)
	$(AR) crv $@ $^

ettclsh : $(LIB_ETTCL).$(LIB_TYPE)

ifeq ($(wildcard .depend),.depend)
include .depend
endif

# *****************************************************************************
# Clean section
# *****************************************************************************

.PHONY : clean

clean :
	rm -f .depend
	rm -f $(TARGETS)
	rm -f ettclsh.o $(SRCS:.c=.o)
	rm -f *~
