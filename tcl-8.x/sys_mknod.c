/* 
 * sys_mknod.c --
 *
 *      Implement the "sys_mknod" command.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

#include "ettcl.h"

/* **************************************************************************
 * Usage:
 *   sys_mknod type name mode [major minor]
 *
 *   type	- the node type: one of 'S_IFREG', 'S_IFCHR' or 'c' or 'u',
 *                'S_IFBLK' or 'b', S_IFIFO or 'p'
 *   name	- the new file name
 *   mode	- access mode for the new file
 *   major	- major number for new device special file
 *   minor	- minor number for new device special file
 * ************************************************************************** */
int ettcl_sys_mknod(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   mode_t ftype = 0;
   dev_t dev = 0;
   int mode;
   int major, minor;

   if (objc < 4) {
      Tcl_WrongNumArgs(interp, 1, objv, "type name mode ?major minor?");
      return TCL_ERROR;
   }

   if (strcasecmp(Tcl_GetString(objv[1]), "S_IFREG") == 0)
      ftype = S_IFREG;
   else if ((strcasecmp(Tcl_GetString(objv[1]), "S_IFCHR") == 0) ||
       (strcasecmp(Tcl_GetString(objv[1]), "c") == 0) ||
       (strcasecmp(Tcl_GetString(objv[1]), "u") == 0))
      ftype = S_IFCHR;
   else if ((strcasecmp(Tcl_GetString(objv[1]), "S_IFBLK") == 0) ||
       (strcasecmp(Tcl_GetString(objv[1]), "b") == 0))
      ftype = S_IFBLK;
   else if ((strcasecmp(Tcl_GetString(objv[1]), "S_IFIFO") == 0) ||
       (strcasecmp(Tcl_GetString(objv[1]), "p") == 0))
      ftype = S_IFIFO;
   else {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": unknow file type", NULL);
      return TCL_ERROR;
   }

   if ((((ftype == S_IFCHR) || (ftype == S_IFBLK)) && objc != 6) ||
        objc != 4) {
      Tcl_WrongNumArgs(interp, 1, objv, "type name mode ?major minor?");
      return TCL_ERROR;
   }

   /* Get the access mode */
   if (Tcl_GetIntFromObj(interp, objv[3], &mode) != TCL_OK) {
      Tcl_AppendResult(interp, "\nwhile parsing file mode", NULL);
      return TCL_ERROR;
   }

   if ((ftype == S_IFCHR) || (ftype == S_IFBLK)) {
      /* In this case we need also the major and minor number */
      if (Tcl_GetIntFromObj(interp, objv[4], &major) != TCL_OK) {
         Tcl_AppendResult(interp, "\nwhile parsing major number", NULL);
         return TCL_ERROR;
      }
      if ((major < 0) || (major > 255)) {
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                  ": major number out of range", NULL);
         return TCL_ERROR;
      }
      if (Tcl_GetIntFromObj(interp, objv[5], &minor) != TCL_OK) {
         Tcl_AppendResult(interp, "\nwhile parsing major number", NULL);
         return TCL_ERROR;
      }
      if ((minor < 0) || (minor > 255)) {
         Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                                  ": minor number out of range", NULL);
         return TCL_ERROR;
      }

      dev  = (major << 8);
      dev |= minor;
   }

   mode |= ftype;

   if (mknod(Tcl_GetString(objv[2]), mode, dev) != 0) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": ", strerror(errno), NULL);
      return TCL_ERROR;
   }

   return TCL_OK;                                                        
}
