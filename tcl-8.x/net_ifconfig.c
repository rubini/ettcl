/* 
 * sys_ifconfig.c --
 *
 *      Implement the "ifconfig" command.
 *
 * Copyright (c) 1998,1999,2001    Alessandro Rubini <rubini@linux.it>
 * Copyright (c) 2002,2003         Rodolfo Giometti <giometti@linux.it>
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "ettcl.h"

struct device_info {
   struct ifreq req;
   int flags;
};

/* **************************************************************************
 * Show device configuration
 * ************************************************************************** */
static int ifconfig_show(int fd, ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   struct device_info *dv = (struct device_info *) clientData;
   char string[] = " hwaddr %02x:%02x:%02x:%02x:%02x:%02x ";

   Tcl_AppendResult(interp,  dv->req.ifr_name, ": ",
                             dv->flags&IFF_UP ? "up" : "down", NULL);

   if (ioctl(fd, SIOCGIFADDR, &dv->req) < 0) {
      Tcl_ResetResult(interp);
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": cannot get device address", NULL);
      return TCL_ERROR;
   }
   Tcl_AppendResult(interp, " addr ",
                            inet_ntoa((*(struct sockaddr_in *)
                                  &dv->req.ifr_addr).sin_addr), NULL);

   if (ioctl(fd, SIOCGIFNETMASK, &dv->req) < 0) {
      Tcl_ResetResult(interp);
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": cannot get device mask", NULL);
      return TCL_ERROR;
   }
   Tcl_AppendResult(interp, " mask ",
                            inet_ntoa((*(struct sockaddr_in *)
                                  &dv->req.ifr_netmask).sin_addr), NULL);

   if (ioctl(fd, SIOCGIFBRDADDR, &dv->req) < 0) {
      Tcl_ResetResult(interp);
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": cannot get device bradcast address", NULL);
      return TCL_ERROR;
   }
   Tcl_AppendResult(interp, " bcast ",
                            inet_ntoa((*(struct sockaddr_in *)
                                  &dv->req.ifr_broadaddr).sin_addr), NULL);

   if (ioctl(fd, SIOCGIFDSTADDR, &dv->req) < 0) {
      Tcl_ResetResult(interp);
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": cannot get device pointopoint address", NULL);
      return TCL_ERROR;
   }
   Tcl_AppendResult(interp, " pointopoint ",
                            inet_ntoa((*(struct sockaddr_in *)
                                  &dv->req.ifr_dstaddr).sin_addr), NULL);

   if (ioctl(fd, SIOCGIFHWADDR, &dv->req) < 0) {
      Tcl_ResetResult(interp);
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": cannot get device hardware address", NULL);
      return TCL_ERROR;
   }
   sprintf(string, " hwaddr %02x:%02x:%02x:%02x:%02x:%02x ",
		    dv->req.ifr_hwaddr.sa_data[0] & 0xff,
		    dv->req.ifr_hwaddr.sa_data[1] & 0xff,
		    dv->req.ifr_hwaddr.sa_data[2] & 0xff,
		    dv->req.ifr_hwaddr.sa_data[3] & 0xff,
		    dv->req.ifr_hwaddr.sa_data[4] & 0xff,
		    dv->req.ifr_hwaddr.sa_data[5] & 0xff);
   Tcl_AppendResult(interp, string, NULL);

   if (ioctl(fd, SIOCGIFMTU, &dv->req) < 0) {
      Tcl_ResetResult(interp);
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": cannot get device mtu", NULL);
      return TCL_ERROR;
   }
   sprintf(string," mtu %i ", (int) dv->req.ifr_mtu);
   Tcl_AppendResult(interp, string, NULL);

   return TCL_OK;
}

/* **************************************************************************
 * "up" or "down" subcommands
 *
 * Usage:
 *   ifconfig dev up 
 *   ifconfig dev down
 *
 *   dev	- the network device to config
 * ************************************************************************** */
static int ifconfig_updown(int fd, ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   struct device_info *dv = (struct device_info *) clientData;

   if (*Tcl_GetString(objv[2]) == 'u')   /* up */
      dv->flags |= IFF_UP;
   else					 /* down */
      dv->flags &= ~IFF_UP;
   dv->req.ifr_flags = dv->flags;
   if (ioctl(fd, SIOCSIFFLAGS, &dv->req) < 0) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": cannot set up devide", NULL);
      return TCL_ERROR;
   }

   return TCL_OK;
}

/* **************************************************************************
 * "addr", "netmask", "broadcast" or "pointopoint" subcommands
 *
 * Usage:
 *   ifconfig dev addr address
 *   ifconfig dev netmask address
 *   ifconfig dev broadcast address
 *   ifconfig dev pointopoint address
 *
 *   dev	- the network device to config
 *   address	- the address to be set up
 * ************************************************************************** */
static int ifconfig_set_addr(int fd, ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   struct device_info *dv = (struct device_info *) clientData;
   struct in_addr addr;
   int ioc;

   if (!inet_aton(Tcl_GetString(objv[3]), &addr)) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": invalid address", NULL);
      return TCL_ERROR;
   }

   if (*Tcl_GetString(objv[2]) == 'a') {        /* addr */
      ioc = SIOCSIFADDR;
      (*(struct sockaddr_in *) &dv->req.ifr_addr).sin_addr = addr;
      dv->req.ifr_addr.sa_family = AF_INET;
   }
   else if (*Tcl_GetString(objv[2]) == 'n') {   /* netmask */
      ioc = SIOCSIFNETMASK;
      (*(struct sockaddr_in *) &dv->req.ifr_netmask).sin_addr = addr;
      dv->req.ifr_netmask.sa_family = AF_INET;
   }
   else if (*Tcl_GetString(objv[2]) == 'b') {   /* broadcast */
      ioc = SIOCSIFBRDADDR;
      (*(struct sockaddr_in *) &dv->req.ifr_broadaddr).sin_addr = addr;
      dv->req.ifr_broadaddr.sa_family = AF_INET;
   }
   else {					/* pointopoint */
      ioc = SIOCSIFDSTADDR;
      (*(struct sockaddr_in *) &dv->req.ifr_dstaddr).sin_addr = addr;
      dv->req.ifr_dstaddr.sa_family = AF_INET;
   }

   if (ioctl(fd, ioc, &dv->req) < 0) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": cannot set up ", Tcl_GetString(objv[2]),
                               " address", NULL);
      return TCL_ERROR;
   }

   return TCL_OK;
}

/* **************************************************************************
 * Usage:
 *   ifconfig dev [cmd args]
 *
 *   dev	- the network device to config
 *   cmd	- special ifconfig subcommand
 *   args 	- arguments for specified subcommand
 * ************************************************************************** */
int ettcl_net_ifconfig(ClientData clientData, Tcl_Interp *interp, int objc, Tcl_Obj *CONST objv[])
{
   int i;
   CONST84 char *cmds[] = {
      "up", "down", "addr", NULL
   };
   int fd;
   struct device_info dv;
   int ret = TCL_OK;

   if (objc < 2) {
      Tcl_WrongNumArgs(interp, 1, objv, "dev ?cmd args?");
      return TCL_ERROR;
   }

   /* Decode the command */
   if (objc == 2)   /* no command, must show configuration */
      i = -1;
   else if (Tcl_GetIndexFromObj(interp, objv[2], cmds, "subcommand", 0, &i) != TCL_OK)
      return TCL_ERROR;

   /* We need a raw socket */
   if ((fd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]), 
                               ": ", strerror(errno), NULL);
      return TCL_ERROR;
   }

   /* Now get device info and store some usefull pointers */
   strncpy(dv.req.ifr_name, Tcl_GetString(objv[1]), IFNAMSIZ-1);   /* ifname */
   if (ioctl(fd, SIOCGIFFLAGS, &dv.req) < 0) {
      Tcl_AppendResult(interp, Tcl_GetString(objv[0]),
                               ": invalid dev", NULL);
      return TCL_ERROR;
   }
   dv.flags = dv.req.ifr_flags;

   /* Do the job */
   switch (i) {
      case -1 : {   /* no commands */
         ret = ifconfig_show(fd, &dv, interp, objc, objv);
         break;
      }
      case 0 :     /* up */
      case 1 : {   /* down */
         ret = ifconfig_updown(fd, &dv, interp, objc, objv);
         break;
      }
      case 2 : {   /* addr */
         ret = ifconfig_set_addr(fd, &dv, interp, objc, objv);
         break;
      }
   }

   close(fd);

   return ret;
}
