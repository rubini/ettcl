
#ifndef _TCL
#include "ettcl.h"
#endif

EXTERN int	Tcl_ProsaSysCmd _ANSI_ARGS_((ClientData clientData,
		    Tcl_Interp *interp, int argc, char **argv));

EXTERN int	Tcl_ProsaCmdCmd _ANSI_ARGS_((ClientData clientData,
		    Tcl_Interp *interp, int argc, char **argv));

EXTERN int	Tcl_ProsaTimeCmd _ANSI_ARGS_((ClientData clientData,
		    Tcl_Interp *interp, int argc, char **argv));

EXTERN int	Tcl_ProsaNetCmd _ANSI_ARGS_((ClientData clientData,
		    Tcl_Interp *interp, int argc, char **argv));

EXTERN int	Tcl_ProsaUdpCmd _ANSI_ARGS_((ClientData clientData,
		    Tcl_Interp *interp, int argc, char **argv));
