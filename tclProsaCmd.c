/* Prosa generic command addons to tclsh for ET-Linux */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <sys/mount.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <sched.h>

#ifdef __i386__ /* direct port I/O is only done on the x86 platform */
#  include <asm/io.h>
#  ifdef __GLIBC__
#    include <sys/perm.h>
# /* define KDSETLED   0x4B32  -- not used any more */
#  else
# /* include <linux/kd.h> -- not used any more */
#    include <linux/fs.h>
#  endif
#else
# /* define KDSETLED   0x4B32  -- not used any more */
#endif

#include "ettclInt.h"
#include "ettclPort.h"

#ifdef TCL_USE_PROSA_EXT

/* tclProsaCmd private typedefs */
/*
 * This structure holds data concerning scheduling 
 * policy and priority
 */
typedef struct scheduler_data {
    /* This actually contains the priority */
    struct sched_param param;
    /* Scheduling policy */
    int policy;
} scheduler_data_t;


/* This structure describes holds data concerning a 
 * single scheduler cmd 
 */
typedef struct scheduler_cmd {
    /* True if sched policy shall be changed */
    int do_change_sched_policy;
    /* True if sched prio shall be changed */
    int do_change_sched_prio;
    /* True if sched policy shall be printed */
    int do_print_sched_policy;
    /* True if sched priority shall be printed */
    int do_print_sched_prio;
    /* True if old situation has to be restored at the end */
    int do_restore;
    /* True if cpu shall be yielded */
    int do_yield_cpu;
    /* True if mlock status shall be changed */
    int do_change_mlock;
    /* True if command shall be executed */
    int do_execute_cmd;
    /* True if we must skip to next cmd line argument */
    int do_continue;
    /* True if we must return */
    int do_return;
    /* Current scheduler data (policy & priority) */
    scheduler_data_t curr_sd;
    /* New scheduler data (policy & priority) */
    scheduler_data_t new_sd;
    /* New mlock status */
    int new_mlock_status;
    /* Command */
    char *cmd;
    /* 
     * Index of argument currently scanned 
     * (no options, just <type> and <cmd>)
     */
    int nonooption_arg_index;
    /* Number of arguments */
    int n_args;
    /* Index of argument currently scanned */
    int arg_index;
    /* This is needed by Tclxxx functions */
    Tcl_Interp *interp;
    /* Pointer to arg strings */
    char **argv;
} scheduler_cmd_t;

/* 
 * scheduler command line scanning function (and pointer to it)
 */
typedef int scheduler_cmd_sc_fun(scheduler_cmd_t *);
typedef scheduler_cmd_sc_fun *pscheduler_cmd_sc_fun;



/*
 * buffer and procedures used by uuencode and uudecode
 */

#define UUBUFLEN 80
static unsigned char uubuffer[UUBUFLEN];
void uuencode_this(unsigned char *buffer, int c)
{
    static int ptr;
    static int count = 0;
    unsigned long l;

    if (ptr==0) ptr=1;
    if (c) {
	l = buffer[0]<<16 | buffer[1]<<8 | buffer[2];
	/* this uses '`' instead of ' '  */
	uubuffer[ptr++] = ' ' + (((l>>18)-1) & 0x3f) + 1;
	uubuffer[ptr++] = ' ' + (((l>>12)-1) & 0x3f) + 1;
	uubuffer[ptr++] = ' ' + (((l>> 6)-1) & 0x3f) + 1;
	uubuffer[ptr++] = ' ' + (((l>> 0)-1) & 0x3f) + 1;
	count += c;
    }
    if (count==45 || (count && c<3)) { /* if 0, don't print anything */
	uubuffer[0] = ' '+count;
	uubuffer[ptr] = '\0';
	printf("%s\n",uubuffer);
	count=0; ptr=1;
    }
}

void uudecode_this(FILE *f)
{
    unsigned long l;
    int ptr, count;
    if (uubuffer[0]=='`') return;
    count = uubuffer[0]-' ';
    for (ptr = 1; count; ptr+=4) {
	l = (((uubuffer[ptr]-' ')&0x3f) << 18)
	    | (((uubuffer[ptr+1]-' ')&0x3f) << 12)
	    | (((uubuffer[ptr+2]-' ')&0x3f) << 6)
	    | ((uubuffer[ptr+3]-' ')&0x3f);
	if (count) {putc(l>>16, f); count--;}
	if (count) {putc((l>>8) & 0xff , f); count--;}
	if (count) {putc(l&0xff, f); count--;}
    }
}

/* 
 * Auxiliary functions for scheduler command 
 */

/* 
 * scheduler_cmd_mlockall()
 * Change mlock status
 * 
 * Input:   New status
 * Output:  TCL_OK if OK, TCL_ERROR in case of errors
 * Description: call mlockall() or munlockall() depending on new status
 * 
 */
static int scheduler_mlockall(int status) {
    int ret;
    if (status) 
	/* ON */
        ret = mlockall(MCL_CURRENT|MCL_FUTURE);
    else 
	/* OFF */
	ret = munlockall();
    return ret < 0 ? TCL_ERROR : TCL_OK;
}

/* 
 * scheduler_cmd_notify_error()
 * Notify an error
 * 
 * Input:   String to be output before the error message
 * Output:  Always TCL_ERROR
 * Description: call TclAppendResult() with proper parameters
 * 
 */
int scheduler_cmd_notify_error(scheduler_cmd_t *cmd, char *msg)
{
    Tcl_AppendResult(cmd->interp, msg, strerror(errno), NULL);
    return TCL_ERROR;
}

/* 
 * scheduler_cmd_init()
 * Initialize a scheduler cmd structure 
 * 
 * Input:   Pointer to cmd descriptor
 * Output:  none
 * Description: just zero out the whole struct
 * 
 */
static void scheduler_cmd_init(scheduler_cmd_t *cmd, Tcl_Interp *interp, 
			       int n_args, char **argv) 
{
    memset(cmd, 0, sizeof(scheduler_cmd_t));
    cmd->interp = interp;
    cmd->n_args = n_args;
    cmd->argv = argv;
}


/* 
 * scheduler_cmd_get_curr_sched_data()
 * Read current scheduler policy and prio, and store them 
 * in the cmd descriptor 
 *
 * Input: Pointer to cmd descriptor 
 * Output: TCL_OK if OK, TCL_ERROR in case of errors 
 * Description: read current sched policy and param and store them
 *              in the cmd descriptor 
 */
static int scheduler_cmd_get_curr_sched_data(scheduler_cmd_t *cmd)
{
    /* Get the current scheduler policy */
    if ((cmd->curr_sd.policy = sched_getscheduler(0)) < 0) 
	return scheduler_cmd_notify_error(cmd, "scheduler: ");
    /* Get the current priority */
    if (sched_getparam(0, &cmd->curr_sd.param) < 0)
	return scheduler_cmd_notify_error(cmd, "scheduler: ");
    if(cmd->n_args==1)
	cmd->do_print_sched_policy = 1;
    return TCL_OK;
}

/* 
 * scheduler_cmd_check_sched_prio_change()
 * Check if given cmd. line par. concerns scheduler prio
 *
 * Input:  Pointer to cmd descriptor 
 * Output: TCL_OK if OK, TCL_ERROR in case of errors 
 * Description: look for -priority and possibly new priority value
 *              Store them into cmd descriptor
 */
static int scheduler_cmd_check_sched_prio_change(scheduler_cmd_t *cmd)
{
    int ret, d;
    if (strcmp(cmd->argv[cmd->arg_index], "-priority")) 
	return TCL_OK;
    /* Arg found, skip to next one */
    cmd->do_continue = 1;
    if (cmd->arg_index + 1 < cmd->n_args) {
	/* -priority requires one more par */
	cmd->arg_index++;	    /* Read the new priority */
	if (Tcl_GetInt(cmd->interp, cmd->argv[cmd->arg_index], 
		       &d) != TCL_OK)
	    return scheduler_cmd_notify_error(cmd, "scheduler: ");
	cmd->new_sd.param.sched_priority = d;
	cmd->do_print_sched_prio=1;
	cmd->do_change_sched_prio=1;
    } else 
	cmd->do_print_sched_prio=1;
    return TCL_OK;
}

/* 
 * scheduler_cmd_check_cpu_yield()
 * Check if CPU has to be yielded.
 *
 * Input:  Pointer to cmd descriptor 
 * Output: TCL_OK if OK, TCL_ERROR in case of errors 
 * Description: look for -yield. In case we find it, just yield the cpu 
 *              and get the cmd scanning process to finish
 */
static int scheduler_cmd_check_cpu_yield(scheduler_cmd_t *cmd)
{
    if (strcmp(cmd->argv[cmd->arg_index], "-yield")) 
	return TCL_OK;
    cmd->do_return = 1;
    if (sched_yield() < 0)
	return scheduler_cmd_notify_error(cmd, "scheduler: ");
    return TCL_OK;
}

/* 
 * scheduler_cmd_check_mlock_change()
 * Check if mlock has to be toggled
 *
 * Input:  Pointer to cmd descriptor 
 * Output: TCL_OK if OK, TCL_ERROR in case of errors 
 * Description: look for -mlock. In case we find it, look for new 
 *              status (on or off) and set cmd flags properly
 *              
 */
static int scheduler_cmd_check_mlock_change(scheduler_cmd_t *cmd)
{
    int n_mem;
    if (strcmp(cmd->argv[cmd->arg_index], "-mlock")) 
	return TCL_OK;
#if 0 /* Does not influence printing of scheduling policy */
    cmd->do_print_sched_policy = 0; /* signal to return no old policy */
#endif
    /* mlock requires one more arg */
    if (cmd->arg_index + 1 < cmd->n_args) {   
	cmd->arg_index++;
	/* Read new memory status */
	if (Tcl_GetBoolean(cmd->interp, cmd->argv[cmd->arg_index], 
			   &n_mem) != TCL_OK)
	    return scheduler_cmd_notify_error(cmd, "scheduler: ");
	cmd->do_change_mlock = 1;
    }
    else {   /* no more parameters */
	Tcl_SetResult(cmd->interp, "scheduler: wrong # args", TCL_STATIC);
	return TCL_ERROR;
    }
    cmd->do_continue = 1;
    cmd->new_mlock_status = n_mem;
    return TCL_OK;
}

/* 
 * scheduler_cmd_check_sched_policy_change()
 * Check if scheduling policy has to be changed 
 *
 * Input:  Pointer to cmd descriptor 
 * Output: TCL_OK if OK, TCL_ERROR in case of errors 
 * Description: if nonooption_arg_index > 0, bail out.
 *              otherwise look for "OTHER", "FIFO" and "RR"
 *              In case we find one, set new scheduling policy
 *              
 */
static int scheduler_cmd_check_sched_policy_change(scheduler_cmd_t *cmd)
{
    int n_pol;
    if (cmd->nonooption_arg_index) 
	return TCL_OK;
    cmd->do_print_sched_policy = 1;
    cmd->do_change_sched_policy = 1;
    cmd->nonooption_arg_index++;
    /* Read the new scheduling policy */
    if (!strcmp(cmd->argv[cmd->arg_index], "OTHER"))
	n_pol = SCHED_OTHER;
    else if (!strcmp(cmd->argv[cmd->arg_index], "FIFO"))
	n_pol = SCHED_FIFO;
    else if (!strcmp(cmd->argv[cmd->arg_index], "RR"))
	n_pol = SCHED_RR;
    else {
	Tcl_SetResult(cmd->interp, "scheduler: invalid scheduler policy",
		      TCL_STATIC);
	return TCL_ERROR;
    }
    cmd->new_sd.policy = n_pol;
    cmd->do_continue = 1;
    return TCL_OK;
}

/* 
 * scheduler_cmd_check_sched_policy_change()
 * Check if a command has to be executed 
 *
 * Input:  Pointer to cmd descriptor 
 * Output: TCL_OK if OK, TCL_ERROR in case of errors 
 * Description: if nonooption_arg_index != 1, bail out.
 *              store command to be executed and signal not to
 *              print either old policy or priority
 *              
 */
static int scheduler_cmd_check_cmd(scheduler_cmd_t *cmd)
{
    if (cmd->nonooption_arg_index != 1) 
	return TCL_OK;
    cmd->do_execute_cmd = 1;
    cmd->do_print_sched_policy = 0;
    cmd->do_print_sched_prio = 0;
    /* Get the command to execute */
    cmd->cmd = cmd->argv[cmd->arg_index];
    return TCL_OK;
}

/* 
 * scheduler_cmd_do_change_scheduler()
 * Change scheduler priority and policy (if needed)
 *
 * Input:  Pointer to cmd descriptor
 * Output: TCL_OK if OK, TCL_ERROR in case of errors 
 * Description: sets a new policy or priority if needed.
 *              Note that sched_setscheduler() sets both the policy 
 *              and the priority ! If only a new priority has to be 
 *              set, we must make sure that new_policy = old_policy
 *              
 */
static int scheduler_cmd_do_change_scheduler(scheduler_cmd_t *cmd)
{
    int priomin, priomax;
    if(!cmd->do_change_sched_policy && !cmd->do_change_sched_prio)
	return TCL_OK;
    /* Check if we have to set a new scheduling policy */
    if (cmd->do_change_sched_policy) {
	priomin = sched_get_priority_min(cmd->new_sd.policy);
	priomax = sched_get_priority_max(cmd->new_sd.policy);
	if(cmd->new_sd.param.sched_priority < priomin || 
	   cmd->new_sd.param.sched_priority > priomax) {
	    Tcl_SetResult(cmd->interp, "scheduler: invalid priority for"
			  "new scheduler policy",
		      TCL_STATIC);
	    return TCL_ERROR;
	}
	if (sched_setscheduler(0, cmd->new_sd.policy, &cmd->new_sd.param) < 0) 
	    return scheduler_cmd_notify_error(cmd, "scheduler: ");
	return TCL_OK;
    }
    /* Check if we have to set a new priority */
    else if (cmd->do_change_sched_prio) {
	if (sched_setparam(0, &cmd->new_sd.param) < 0) 
	    return scheduler_cmd_notify_error(cmd, "scheduler: ");
    }
    return TCL_OK;
}

/* 
 * scheduler_cmd_do_toggle_mlock()
 * Toggle mlock status (if needed)
 *
 * Input:  Pointer to cmd descriptor
 * Output: TCL_OK if OK, TCL_ERROR in case of errors 
 * Description: just toggle mlock status if do_change_mlock
 *              
 */
static int scheduler_cmd_do_toggle_mlock(scheduler_cmd_t *cmd)
{
    if(!cmd->do_change_mlock)
	return TCL_OK;
    return scheduler_mlockall(cmd->new_mlock_status);
}

/* 
 * scheduler_cmd_do_exec_cmd()
 * Execute cmd (if needed)
 *
 * Input:  Pointer to cmd descriptor
 * Output: TCL_OK if OK, TCL_ERROR in case of errors 
 * Description: execute command (if needed) and then restore old 
 *              status.
 *              
 */
static int scheduler_cmd_do_exec_cmd(scheduler_cmd_t *cmd)
{
    int ret;
    if (!cmd->do_execute_cmd) 
	return TCL_OK;
    ret = Tcl_Eval(cmd->interp, cmd->cmd);	
    /* Reset the old status */
    if (sched_setscheduler(0, cmd->curr_sd.policy, 
			   &cmd->curr_sd.param) < 0) 
	return scheduler_cmd_notify_error(cmd, "scheduler: ");
    
    if(cmd->do_change_mlock) {
	/* We changed the mlock status. We need to toggle it now */
	cmd->new_mlock_status = !cmd->new_mlock_status;
	if(scheduler_cmd_do_toggle_mlock(cmd) < 0)
	    return scheduler_cmd_notify_error(cmd,  "scheduler: ");
    }
    /* Return the <cmd> exit code */
    return ret;    
}

/* 
 * scheduler_cmd_do_exec_print()
 * Print what is needed
 *
 * Input:  Pointer to cmd descriptor
 * Output: TCL_OK if OK, TCL_ERROR in case of errors 
 * Description: print info according to cmd flags
 *              
 */
static int scheduler_cmd_do_print(scheduler_cmd_t *cmd)
{
    char string[24] = "";
    if (cmd->do_print_sched_prio)
	sprintf(string, 
		cmd->do_print_sched_policy ? "%d " : "%d", 
		cmd->curr_sd.param.sched_priority);
    if (cmd->do_print_sched_policy) {
	switch (cmd->curr_sd.policy) {
	case SCHED_OTHER:
	    sprintf(&string[strlen(string)], "OTHER"); 
	    break;
	case SCHED_FIFO:
	    sprintf(&string[strlen(string)], "FIFO"); 
	    break;
	case SCHED_RR:
	    sprintf(&string[strlen(string)], "RR"); 
	    break;
	}
    }
    Tcl_SetResult(cmd->interp, string, TCL_VOLATILE);
    return TCL_OK;
}

/*
 * Simple commands of general utility
 */

int Tcl_ProsaCmdCmd(ClientData dummy, Tcl_Interp *interp, 
		    int argc, char **argv)
{
    int i, j;
    static char string[16], dir[16], fstype[16];
    extern char globalargv0[];
    int fd;
    FILE *f;
    int d;

    switch(argv[0][0]) {

        /*
         * "inp" and "inw" receive a port number on cmdline (hex or dec)
         * and returns the current input value.
         */
      case 'i':                                         /* in[pw] */
        if (argc!=2) goto wna;
        if (Tcl_GetInt(interp, argv[1], &i) != TCL_OK) {
            Tcl_AppendResult(interp, "\nwhile parsing a port number",
                             NULL);
            return TCL_ERROR;
        }
#ifdef __i386__ /* use direct instruction, but require access first */
        if (ioperm(i,2,1)) { /* ask for two ports, in case inw is used */
            Tcl_AppendResult(interp, argv[0], ": ioperm(): ", strerror(errno),
                             NULL);
            return TCL_ERROR;
        }
	if (argv[0][2]=='p') { /* simple 8-bit port */
            j = inb(i);
            sprintf(string,"0x%02x",j);
        } else {
            j = inw(i);
            sprintf(string,"0x%04x",j);
        }
#else /* not 386, use /dev/port */
	fd = open("/dev/port", O_RDONLY);
	if (fd < 0) {
            Tcl_AppendResult(interp, argv[0], ": /dev/port: ", strerror(errno),
                             NULL);
            return TCL_ERROR;
        }
	lseek(fd, i, SEEK_SET);
	if (argv[0][2]=='p') { /* simple 8-bit port */
	    unsigned char c;
            read(fd, &c, 1); j = c;
            sprintf(string,"0x%02x",j);
        } else {
	    unsigned short w;
            read(fd, &w, 2); j = w;
            sprintf(string,"0x%04x",j);
        }
	close(fd);
#endif
        Tcl_SetResult(interp,string,TCL_VOLATILE);
        return TCL_OK;

        /*
         * "outp" and "outtw" receive a port number and a byte/word value
         * and calls an outb or outw operation.
         */
      case 'o':                                         /* outp */
        if (argc!=3) goto wna;
        if (Tcl_GetInt(interp, argv[1], &i) != TCL_OK) {
            Tcl_AppendResult(interp, "\nwhile parsing a port number",
                             NULL);
            return TCL_ERROR;
        }
        if (Tcl_GetInt(interp, argv[2], &j) != TCL_OK) {
            Tcl_AppendResult(interp, "\nwhile parsing a byte/word value",
                             NULL);
            return TCL_ERROR;
        }
#ifdef __i386__
        if (ioperm(i,2,1)) { /* ask for twwo bytes, in case outw is used */
            Tcl_AppendResult(interp, argv[0], ": ioperm(): ", strerror(errno),
                             NULL);
            return TCL_ERROR;
        }
        if (argv[0][3]=='b') 
            outb(j,i);
        else
            outw(j,i);
#else /* not __i386__ */
	fd = open("/dev/port", O_RDWR);
	if (fd < 0) {
            Tcl_AppendResult(interp, argv[0], ": /dev/port: ", strerror(errno),
                             NULL);
            return TCL_ERROR;
        }
	lseek(fd, i, SEEK_SET);
	if (argv[0][2]=='p') { /* simple 8-bit port */
	    unsigned char c;
	    c = j; write(fd, &c, 1);
        } else {
	    unsigned short w;
            w = j; write(fd, &w, 2);
        }
	close(fd);
#endif
        return TCL_OK;

        /*
         * "readb", "readw", "readl" receive a memory address on
         * cmdline (hex or dec) and return the current content.
	 */

      case 'r':                                         /* read[bwl] */
        if (argc!=2) goto wna;
        if (Tcl_GetInt(interp, argv[1], &i) != TCL_OK) {
            Tcl_AppendResult(interp, "\nwhile parsing a memory address",
                             NULL);
            return TCL_ERROR;
        }
	fd = open("/dev/mem", O_RDONLY);
	if (fd < 0) {
            Tcl_AppendResult(interp, argv[0], ": /dev/mem: ", strerror(errno),
                             NULL);
            return TCL_ERROR;
        }
	lseek(fd, i, SEEK_SET);
	if (argv[0][4]=='b') {
	    unsigned char c;
            read(fd, &c, 1); j = c;
            sprintf(string,"0x%02x",j);
        } else if (argv[0][4]=='w') {
	    unsigned short w;
            read(fd, &w, 2); j = w;
            sprintf(string,"0x%04x",j);
        } else /* long */ {
	    unsigned int l; /* long may be 64-bit. Int is 32, hopefully */
            read(fd, &l, 4); j = l;
            sprintf(string,"0x%08x",j);
	}
	close(fd);
        Tcl_SetResult(interp,string,TCL_VOLATILE);
        return TCL_OK;

        /*
         * "writeb", "writew" and "writel" receive an address and a value,
	 * and write that value to memory.
         */
      case 'w':                                         /* write[bwl] */
        if (argc!=3) goto wna;
        if (Tcl_GetInt(interp, argv[1], &i) != TCL_OK) {
            Tcl_AppendResult(interp, "\nwhile parsing a port number",
                             NULL);
            return TCL_ERROR;
        }
        if (Tcl_GetInt(interp, argv[2], &j) != TCL_OK) {
            Tcl_AppendResult(interp, "\nwhile parsing a byte/word value",
                             NULL);
            return TCL_ERROR;
        }
	fd = open("/dev/mem", O_RDWR);
	if (fd < 0) {
            Tcl_AppendResult(interp, argv[0], ": /dev/mem: ", strerror(errno),
                             NULL);
            return TCL_ERROR;
        }
	lseek(fd, i, SEEK_SET);
	if (argv[0][4]=='b') {
	    unsigned char c = j;
            write(fd, &c, 1);
        } else if (argv[0][4]=='w') {
	    unsigned short w = j;
            write(fd, &w, 2);
        } else /* long */ {
	    unsigned int l=j; /* long may be 64-bit. Int is 32, hopefully */
            write(fd, &l, 4);
	}
	close(fd);
        return TCL_OK;


#if 0 /* setleds was an horrible hack. Removed */

        /*
         * "setleds" is passed a file handle and a number, and calls ioctl()
         * to toggle the lights 
         */
      case 's':                                         /* setleds */
        if (argc!=3) goto wna;

        if (Tcl_GetInt(interp, argv[2], &i) != TCL_OK) {
            Tcl_AppendResult(interp, "\nwhile parsing a port number",
                             NULL);
            return TCL_ERROR;
        }
        chanPtr = Tcl_GetChannel(interp, argv[1], NULL);
        if (chanPtr == (Tcl_Channel) NULL) {
            return TCL_ERROR;
        }
        fsPtr = Tcl_GetChannelInstanceData(chanPtr);
        if (fsPtr->inFile) {
            fd = (int) Tcl_GetFileInfo(fsPtr->inFile, NULL);
        } else {
            fd = (int) Tcl_GetFileInfo(fsPtr->outFile, NULL);
        }
        if (ioctl(fd,KDSETLED,(long)i)) {
            Tcl_AppendResult(interp, "setleds: ioctl(): ", strerror(errno),
                             NULL);
            return TCL_ERROR;
        }
        return TCL_OK;
#endif /* if 0 */
        /*
         * "scheduler" modify the scheduling policy, possibly 
	 * execute command and restore old status 
         */
    case 's': {
	scheduler_cmd_t cmd;
	int ret, i;
	static const pscheduler_cmd_sc_fun scfun[] = {
	    scheduler_cmd_check_sched_prio_change,
	    scheduler_cmd_check_cpu_yield,
	    scheduler_cmd_check_mlock_change,
	    scheduler_cmd_check_sched_policy_change,
	    scheduler_cmd_check_cmd,
	    NULL
	};
	
	/* Initialize and read current scheduler data */
	scheduler_cmd_init(&cmd, interp, argc, argv);
	if(scheduler_cmd_get_curr_sched_data(&cmd)==TCL_ERROR)
	    return TCL_ERROR;
	/* Preset new scheduler data to old values */
	memcpy(&cmd.new_sd, &cmd.curr_sd, sizeof(scheduler_data_t));
	/* Parse the parameters */
	for (cmd.arg_index = 1; cmd.arg_index < argc; cmd.arg_index++) {
	    cmd.do_continue = 0;
	    for(i=0; ;i++) {
		if(!scfun[i])
		    break;
		if(scfun[i](&cmd) < 0)
		    return TCL_ERROR;
		/* If do_continue, we must skip to the next arg */
		if(cmd.do_continue)
		    break;
		if(cmd.do_return)
		    return TCL_OK;
	    }
	}
	/* Ok, now we do the job */
	if(scheduler_cmd_do_change_scheduler(&cmd)==TCL_ERROR)
	    return TCL_ERROR;
	if(scheduler_cmd_do_toggle_mlock(&cmd)==TCL_ERROR)
	    return TCL_ERROR;
	if(cmd.do_execute_cmd)
	    return scheduler_cmd_do_exec_cmd(&cmd);
	/* Now we just have to print what has to be printed */
	return scheduler_cmd_do_print(&cmd);
    }	
    
    /*
     * "newcmdname" should modify the "Name:" field in /proc/self/status
     */
    case 'n':                                         /* newcmdname */
        if (argc!=2) goto wna;
	strncpy(globalargv0,argv[1],15); /* not works */
        /*
         * Try to do it through /var/run instead
         */
        sprintf(string,"/var/run/%i",getpid());
        f=fopen(string,"w");
        if (f) {
            fprintf(f,"%s\n",argv[1]);
            fclose(f);
        }
        return TCL_OK;

        /*
         * "mount": no arguments to remount root, "mount proc" to mount /proc
         * The first three words in /etc/fstab must be "<blkdev> / <type>"
         */
      case 'm':                                         /* mount */
        if (argc==1) {
            f=fopen("/etc/fstab","r");
            if (!f) {
                Tcl_AppendResult(interp, argv[0], ": /etc/fstab: ",
                                 strerror(errno), NULL);
                return TCL_ERROR;
            }
            i=fscanf(f,"%s %s %s",string,dir,fstype);
            fclose(f);
            if (i!=3) {
                Tcl_AppendResult(interp, argv[0], ": /etc/fstab: wrong data",
                                 NULL);
                return TCL_ERROR;
            }
            if (mount(string, dir, fstype,MS_REMOUNT | MS_MGC_VAL, NULL)) {
                Tcl_AppendResult(interp, argv[0], ": mount(): ",
                                 strerror(errno), NULL);
                return TCL_ERROR;
            }
            return TCL_OK;
        }
        if (argc==2) {
            if (mount("none","/proc","proc", MS_MGC_VAL, NULL)) {
                Tcl_AppendResult(interp, argv[0], ": mount(): ",
                                 strerror(errno), NULL);
                return TCL_ERROR;
            }
        return TCL_OK;
        }
        goto wna;

      case 'u':
	if (argv[0][1]=='m') {                /* umount */
	    sync();
	    umount("/proc");
	    umount("/");
	    return TCL_OK;
	}
	if (argv[0][2]=='e') {                /* uuencode */
	    unsigned char buffer[4];
	    if (argc!=2) goto wna;
	    if (!(f=fopen(argv[1],"r"))) {
		Tcl_AppendResult(interp, argv[0], ": ",argv[1],": ",
				 strerror(errno), NULL);
		return TCL_ERROR;
	    }
	    printf("begin 644 %s\n",argv[1]);
	    while ( (i=fread(buffer,1,3,f)) > 0)
		uuencode_this(buffer, i);
	    uuencode_this(buffer, 0);
	    printf("`\nend\n");
	    fflush(stdout); /* just in case... */
	    return TCL_OK;
	}
	if (argv[0][2]=='d') {                /* uudecode */
	    int mode;
	    char filename[UUBUFLEN];

	    f = NULL;
	    while (fgets(uubuffer, UUBUFLEN, stdin)) {
		if (!strncmp(uubuffer, "end", 3)) {
		    if (f) {
			fclose(f);
			chmod(filename,mode);
		    }
		    return TCL_OK;
		}
		if (!strncmp(uubuffer, "begin", 5)) {
		    if (sscanf(uubuffer, "begin %o %s",&mode,filename)!=2) {
			Tcl_AppendResult(interp, argv[0], ": error in data\n",
					 NULL);
			return TCL_ERROR;
		    }
		    if (!(f=fopen(filename,"w"))) {
			Tcl_AppendResult(interp, argv[0], ": ",uubuffer,": ",
					 strerror(errno), NULL);
			return TCL_ERROR;
		    }
		    continue;
		}
		if (!f) continue;
		uudecode_this(f);
	    }
	    if (f) {
		Tcl_AppendResult(interp, argv[0], ": incomplete input,\"",
				 filename, "\" is corrupted",NULL);
		fclose(f);
		return TCL_ERROR;
	    }
	    return TCL_OK;
	}


      default:
        Tcl_SetResult(interp, "Unknown command", TCL_STATIC);
        return TCL_ERROR;
    }

  wna:
    Tcl_AppendResult(interp, argv[0], ": Wrong number of arguments\n",
                     NULL);
    return TCL_ERROR;
}

int Tcl_ProsaTimeCmd(ClientData dummy, Tcl_Interp *interp, 
		    int argc, char **argv)
{
    /*
     * The command behaves in the following way:
     *
     *    xtime                           returns the time as %li
     *    xtime -milli                    returns the time as %li.%03li
     *    xtime -micro                    returns the time as %li.%06li
     *    xtime -format <fmt> [<time_t>]  returns the time as strftime(<fmt>)
     *    xtime -http [<time_t>]          uses the std http format and GMT
     *    xtime -diff <time>      returns the difference (same fmt as <time>)
     *    xtime -timeto <time>    returns the difference in milliseconds
     *    xtime -wait <tvar> <step>       waits untile $tvar+step, updates tvar
     *    xtime -split <array> [<time_t>] splits the date in components
     *    xtime -join <array>             joins components into a time_t
     */

    time_t  t=0;
    long tdiff, milli, micro, micro2;
    unsigned long sec, sec2;
    struct timeval tv, tvstep;
    struct  tm      *loc;
    unsigned char *fmt = NULL;
    char *var;
    int args, res;
    /* as a (dirty) shortcut, use an int[] for struct tm in split/join*/
    int *tm = (int *)loc;
    char *tmnames[]={"sec", "min", "hour", "mday", "mon", "year",
		   "wday", "yday", "isdst", NULL};
    #define TMNAMES 10

    enum mode {
	TIME_T = 0,
	TIME_MILLI, TIME_MICRO, TIME_FORMAT, TIME_FORMATNOW, TIME_HTTP,
	TIME_HTTPNOW, TIME_DIFF, TIME_TIMETO, TIME_WAIT,
	TIME_SPLIT, TIME_SPLITNOW, TIME_JOIN
    };

    struct {
	char *opt;
	int mode;
	int narg;
    } *optr, optionlist[] = {
	{"-milli",    TIME_MILLI,     2},
	{"-micro",    TIME_MICRO,     2},
	{"-format",   TIME_FORMATNOW, 3},
	{"-format",   TIME_FORMAT,    4},
	{"-http",     TIME_HTTP,      3},
	{"-http",     TIME_HTTPNOW,   2},
	{"-diff",     TIME_DIFF,      3},
	{"-timeto",   TIME_TIMETO,    3},
	{"-wait",     TIME_WAIT,      4},
	{"-split",    TIME_SPLITNOW,  3},
	{"-split",    TIME_SPLIT,     4},
	{"-join",     TIME_JOIN,      3},
	{NULL,        -1,             0},
    };

    int mode = TIME_T;

    if (argc>1) {
	for (optr = optionlist; optr->opt; optr++)
	    if (!strcmp(argv[1], optr->opt) && optr->narg == argc)
		break;
	if (!optr->opt) {
	    Tcl_SetResult(interp, "xtime: wrong arguments\n", TCL_STATIC);
	    return TCL_ERROR;
	}
	mode = optr->mode;
    }

    /* use uubuffer for the return value */
    switch(mode) {
        case TIME_FORMAT:
	    t = (time_t)atol(argv[3]);
        case TIME_FORMATNOW:
	    fmt = argv[2]; /* and fall through */
        case TIME_T:
	    if (!t) t = time(0);
	    if (fmt) {
		loc = localtime(&t);
		strftime(uubuffer, UUBUFLEN, fmt, loc);
	    } else {
	      sprintf(uubuffer,"%ld",(long)t);
	    }		
	    break;

       case TIME_HTTPNOW:
	   t = time(0); /* and fall through */
       case TIME_HTTP:
	   if (!t) t = (time_t)atol(argv[2]);
	   loc = gmtime(&t);
	   fmt = "%a, %d %b %Y %H:%M:%S GMT";
	   strftime(uubuffer, UUBUFLEN, fmt, loc);
	   break;
	   

       case TIME_MILLI:
       case TIME_MICRO:
	   gettimeofday(&tv, NULL);
	   if (mode == TIME_MILLI) {
	       sprintf(uubuffer, "%ld.%03ld",
		       (long)tv.tv_sec, (long)tv.tv_usec/1000);
	   } else {
	       sprintf(uubuffer, "%ld.%06ld", 
		       (long)tv.tv_sec, (long)tv.tv_usec);
	   }
	   break;

       case TIME_TIMETO: /* always returns in millisecs (can't be negative) */
       case TIME_DIFF:   /* returns same fmt as input (can be negative) */
	   milli = micro = 0;
	   args = sscanf(argv[2],"%ld.%3ld%3ld", &sec, &milli, &micro);
	   if (args<1)
	       goto xtimeerr;
	   /* now convert it in sec.micro */
	   micro = milli * 1000 + micro;
	   /* and now get the difference into tdiff.micro (sec is unsigned) */
	   gettimeofday(&tv, NULL);
	   tdiff = sec - tv.tv_sec;
	   micro = micro - tv.tv_usec;
	   if (micro < 0) {
	       micro += 1000 * 1000;
	       tdiff--;
	   }
	   /*
	    * now, micro is always 0-999999, and tdiff may be negative.
	    * A time of -1.5 is thus represented by tdiff==-2 and micro==5e5
	    * (tdiff is the floor of the decimal time-difference
	    */

	   if (mode == TIME_TIMETO) {
	       milli = tdiff * 1000 + micro/1000;
	       /* a negative time is 0, so no problems with signs */
	       if (milli < 0) milli = 0;
	       sprintf(uubuffer, "%ld", milli);
	       break;
	   }

	   /* This its "-diff", and we have problems with negatives */
	   switch(args) {
	       case 1:  /* print only the seconds as floor(timeandmicro) */
		   sprintf(uubuffer, "%ld", tdiff);
		   break;
		   
	       case 2: /* print seconds and milli */
		   if (tdiff == -1 && micro >= 1000) {
		       /* special case: "-0" is not a conventional int */
		       sprintf(uubuffer, "-0.%03ld", 1000 - micro/1000);
		       break;
		   }
		   if (tdiff < 0 && micro >= 1000) {
		       tdiff++;
		       micro = 1000000 - micro;
		   }
		   sprintf(uubuffer, "%ld.%03ld", tdiff, micro/1000);
		   break;

	       case 3: /* print seconds and micro */
		   if (tdiff == -1 && micro) {
		       /* special case: "-0" is not a conventional int */
		       sprintf(uubuffer, "-0.%06ld", 1000000 - micro);
		       break;
		   }
		   if (tdiff < 0 && micro) {
		       tdiff++;
		       micro = 1000000 - micro;
		   }
		   sprintf(uubuffer, "%ld.%06ld", tdiff,micro);
		   break;
	   }
	   break;

       case TIME_SPLITNOW:
	   t = time(0); /* and fall through */
       case TIME_SPLIT:
	   if (!t) t = (time_t)atol(argv[3]);
	   loc = localtime(&t); tm = (int *)loc; res = 0;
	   loc->tm_year += 1900;
	   /* use uubuffer as temporary storage, as usual. "res" is errorfl */
	   for (mode=0; tmnames[mode]; mode++) {
	       sprintf(uubuffer, "%i", tm[mode]);
	       res += !Tcl_SetVar2(interp, argv[2], tmnames[mode], uubuffer, 0);
	   }
	   if (res) {
	       Tcl_SetResult(interp, "time -split: error in setting vars",
			     TCL_STATIC);
	       return TCL_ERROR;
	   }
	   uubuffer[0]='\0'; /* no result */
	   break;

       case TIME_JOIN:
	   tm = calloc(TMNAMES, sizeof(int));
	   sec = (unsigned long)-1;
	   if (tm) {
	       for (mode = 0; tmnames[mode]; mode++) {
		   fmt = Tcl_GetVar2(interp, argv[2], tmnames[mode], 0);
		   if (fmt)
		       sscanf(fmt,"%i",&tm[mode]);
	       }
	       loc = (struct tm *)tm;
	       if (loc->tm_year >= 1900) loc->tm_year -= 1900;
	       sec = mktime(loc);
	       free(tm);
	   }
	   if (sec == (unsigned long)-1) {
	       Tcl_SetResult(interp,"xtime -join: nomem or other error",
			     TCL_STATIC);
	       return TCL_ERROR;
	   }
	   sprintf(uubuffer,"%li",sec);
	   break;
	   
       case TIME_WAIT:
	   /* 
	    * The "-wait" functionality is meant to help in implementing
	    * repeated actions. It gets two arguments: the name of
	    * a variable and a time difference. The command will wait
	    * till time "$var + diff" and then return. It also updates var
	    * so that the function can be used the next time. Both the
	    * variable and the time difference can be either seconds
	    * (as returned by "xtime"), or milliseconds (as returned by
	    * "xtime -milli") or microseconds (as returned by "xtime -micro").
	    * However, the number of decimal digits in both items must be
	    * the same (if they are not, the variable's one takes precedence
	    *, and the result in undefined (read: "wrong") if the decimals are
	    * anything but 0, 3, 6 digits.
	    *
	    * If the time $var+diff is in the past, the command will
	    * return immediately. If even the next time event ($var + 2*diff)
	    * is in the past, the var will be incremented twice and so on.
	    * On exit from the command, thus, the variable will represent
	    * the most recent timestamp in the past. The return value is the
	    * number of "missed" events: 0 in normal case, and 1 or more
	    * if the command is invoked so late that some even has been
	    * missed.  Thus, the application developer can choose to
	    * either loose missed events or process them late.
	    *
	    * Loosing events:
	    *                  while 1 {
	    *                      xtime -wait var $interval
	    *                      <do stuff>
	    *                  }
	    * 
	    * Late processing:
	    *                  while 1 {
	    *                      set i [xtime -wait var $interval]
	    *                      for {} {$i>=0} {incr i -1} {
	    *                          <do stuff>
	    *                      }
	    *                  }
	    */

	   if (argc != 4) goto xtimeerr;
	   var = Tcl_GetVar(interp, argv[2], TCL_LEAVE_ERR_MSG);
	   if (!var) return TCL_ERROR; /* error already there */

	   /* build tvstep */
	   milli = micro = 0;
	   args = sscanf(argv[3],"%ld.%3ld%3ld", &sec, &milli, &micro);
	   if (args < 1) goto xtimeerr;
	   tvstep.tv_sec = sec;
	   tvstep.tv_usec = milli * 1000 + micro;
	   
	   /* retrieve original time */
	   milli = micro = 0;
	   args = sscanf(var,"%ld.%3ld%3ld", &sec, &milli, &micro);
	   if (args < 1) goto xtimeerr;
	   micro = milli * 1000 + micro;
	   /* increment once */
	   sec += tvstep.tv_sec;
	   micro += tvstep.tv_usec;
	   if (micro >= 1000 * 1000) {
	       micro -= 1000 * 1000;
	       sec++;
	   }

	   res = 1;
	   while (1) {
	       /* retrieve current time */
	       gettimeofday(&tv, NULL);
	       if (tv.tv_sec > sec) break;
	       if (tv.tv_sec == sec && tv.tv_usec >= micro) break;

	       /* sleep and return */
	       tv.tv_sec = sec - tv.tv_sec;
	       tv.tv_usec = micro - tv.tv_usec;
	       if (tv.tv_usec < 0) {
		   tv.tv_usec += 1000 * 1000;
		   tv.tv_sec --;
	       }
	       if (select(0, NULL, NULL, NULL, &tv)<0) {
		   /* accept EINTR, as we might just have reaped a child */
		   if (errno != EINTR) {
		       Tcl_ResetResult(interp);
		       Tcl_AppendResult(interp, "xtime -wait: ",
					strerror(errno), NULL);
		       return TCL_ERROR;
		   }
		   Tcl_AsyncInvoke(NULL, 0);
		   continue;
	       }
	       goto xtimewait_ok;
	    }

	   /*
	    * no need to sleep, but count missed events
	    * here, tv is the current time, tvstep the step,
	    * sec:micro is the time of the first (missed) event
	    * and is also the result to be set in the variable, that is,
	    * the last event we missed.
	    */
	   {
	       /* use a long long to avoid overruns */
	       long long diff; long rest;
	       diff = 1000*1000 * (tv.tv_sec - sec) + tv.tv_usec - micro;
	       res = 1 + diff/(tvstep.tv_sec * 1000*1000 + tvstep.tv_usec);
	       rest = diff%(tvstep.tv_sec * 1000*1000 + tvstep.tv_usec);
	       sec = tv.tv_sec - rest/(1000*1000);
	       micro = tv.tv_usec - rest%(1000*1000);
	       if (micro < 0) {
		   micro += 1000*1000;
		   sec -= 1;
	       }
	   }

        xtimewait_ok:
	   switch(args) {
	       case 1:  /* print only the seconds */
		   sprintf(uubuffer, "%ld", sec);
		   break;
		   
	       case 2: /* print seconds and milli */
		   sprintf(uubuffer, "%ld.%03ld", sec, micro/1000);
		   break;

	       case 3: /* print seconds and micro */
		   sprintf(uubuffer, "%ld.%06ld", sec,micro);
		   break;
	   }
	   Tcl_SetVar(interp, argv[2], uubuffer, 0);
	   sprintf(uubuffer, "%i", res);
    }
    Tcl_SetResult(interp,uubuffer,TCL_VOLATILE);
    return TCL_OK;

 xtimeerr:
    /* argv[1] is valid, as the argc==0 case never jums here */
    Tcl_AppendResult(interp, "xtime ", argv[1], ": Wrong arguments\n", NULL);
    return TCL_ERROR;
}


#endif
