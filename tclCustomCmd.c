/*
 * This file can be modified to include custom code to implement Tcl
 * commands.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

#include "ettclInt.h"
#include "ettclPort.h"

int Tcl_Custom0_Cmd(ClientData dummy, Tcl_Interp *interp,
                    int argc, char **argv)
{

    /* implement here your command */


    /*
     * at last, return TCL_OK or TCL_ERROR. In case of error, explain
     * it as well. By default we return error as nothing is there.
     */
    Tcl_AppendResult(interp, argv[0], ": Command not implemented", NULL);
    return TCL_ERROR;
}

int Tcl_Custom1_Cmd(ClientData dummy, Tcl_Interp *interp,
                    int argc, char **argv)
{

    /* implement here your command */


    /*
     * at last, return TCL_OK or TCL_ERROR. In case of error, explain
     * it as well. By default we return error as nothing is there.
     */
    Tcl_AppendResult(interp, argv[0], ": Command not implemented", NULL);
    return TCL_ERROR;
}
